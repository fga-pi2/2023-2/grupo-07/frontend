/* eslint-disable no-param-reassign */
import axios from 'axios';

const API_URL =
  (import.meta.env.VITE_API_URL as string) ?? 'http://localhost:8080';

export const api = axios.create({
  baseURL: API_URL,
});

api.interceptors.request.use(
  (config) => {
    const token = localStorage.getItem('@DRONE_PI2:token');

    if (token) {
      config.headers.Authorization = `Bearer ${token}`;
    }

    return config;
  },
  (error) => Promise.reject(error),
);
