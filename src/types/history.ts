export interface History {
  id_history: number;
  id_drone: number;
  id_base: number;
  id_user: number;
  loaded_at: string;
}
