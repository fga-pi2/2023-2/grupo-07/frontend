export interface Station {
  id_base: number;
  name: string;
  latitude: string;
  longitude: string;
  is_active: boolean;
  status: 'LIVRE' | 'OCUPADA';
  id_user: number;
  created_at: string;
}
