export interface Drone {
  id_drone: number;
  serie: string;
  status: 'CARREGANDO' | 'EM_USO';
  id_base: number;
  id_user: number;
  created_at: string;
}
