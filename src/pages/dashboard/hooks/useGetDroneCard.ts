import { Dispatch, SetStateAction, useCallback, useState } from 'react';
import { toast } from 'react-toastify';

import { api } from '@config/lib/axios/api';

import { Drone } from '../../../types/drone';

interface useGetDronesCardProps {
  setDrones: Dispatch<SetStateAction<Drone[] | null>>;
}

export function useGetDroneCard({ setDrones }: useGetDronesCardProps) {
  const [isLoading, setIsLoading] = useState(false);

  const handleLoadDrones = useCallback(async () => {
    try {
      setIsLoading(true);

      const response = await api.get<Drone[]>('/drones/');

      setDrones(response.data);
    } catch (err) {
      toast.error(
        'Não foi possível carregar os drones. Tente novamente mais tarde!',
      );
    } finally {
      setIsLoading(false);
    }
  }, [setDrones]);

  return { isLoading, handleLoadDrones };
}
