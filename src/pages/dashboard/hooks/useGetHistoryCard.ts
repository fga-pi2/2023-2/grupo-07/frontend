import { Dispatch, SetStateAction, useCallback, useState } from 'react';
import { toast } from 'react-toastify';

import { api } from '@config/lib/axios/api';

import { History } from '../../../types/history';

interface useGetHistoryCardProps {
  setHistory: Dispatch<SetStateAction<History[] | null>>;
}

export function useGetHistoryCard({ setHistory }: useGetHistoryCardProps) {
  const [isLoading, setIsLoading] = useState(false);

  const handleLoadHistory = useCallback(async () => {
    try {
      setIsLoading(true);

      const response = await api.get<History[]>('/history/');

      setHistory(response.data);
    } catch (err) {
      toast.error(
        'Não foi possível carregar o histórico. Tente novamente mais tarde!',
      );
    } finally {
      setIsLoading(false);
    }
  }, [setHistory]);

  return { isLoading, handleLoadHistory };
}
