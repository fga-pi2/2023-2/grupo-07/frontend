import { Flex, Heading, Text } from '@chakra-ui/react';

import { Station } from '../../../../types/station';

interface DashboardStationCardProps {
  stations: Station[] | null;
}

export function DashboardStationCard({ stations }: DashboardStationCardProps) {
  const totalStations = stations?.length;
  const totalActiveStations = stations?.filter((station) => station.is_active)
    .length;
  const totalChargedStations = stations?.filter(
    (station) => station.status === 'charged',
  ).length;

  return (
    <Flex
      flexDir="column"
      borderRadius="1rem"
      bg="white"
      boxShadow="0px 3.5px 5.5px 0px rgba(0, 0, 0, 0.02)"
      gap="1.56rem"
      justifyContent="center"
      padding="2.06rem 3.38rem"
      maxW="420px"
      w="full"
    >
      <Heading
        as="h4"
        textAlign="left"
        color="#2D3748"
        fontSize="1.5rem"
        lineHeight="140%"
      >
        Painel de Bases
      </Heading>
      <svg
        xmlns="http://www.w3.org/2000/svg"
        width="98"
        height="57"
        viewBox="0 0 98 57"
        fill="none"
      >
        <path
          d="M86.6923 0H18.8462C12.4385 0 7.53846 4.63125 7.53846 10.6875V14.25H3.76923C1.50769 14.25 0 15.675 0 17.8125V39.1875C0 41.325 1.50769 42.75 3.76923 42.75H7.53846V46.3125C7.53846 52.3688 12.4385 57 18.8462 57H86.6923C93.1 57 98 52.3688 98 46.3125V10.6875C98 4.63125 93.1 0 86.6923 0ZM82.9231 32.0625L56.5385 28.5V42.75L26.3846 24.9375L52.7692 28.5V14.25L82.9231 32.0625Z"
          fill="#4FD1C5"
        />
      </svg>
      <Flex flexDir="column" w="100%">
        <Flex justify="flex-start" w="100%">
          <Text
            color="#2D3748"
            fontSize="1.125rem"
            fontWeight="700"
            lineHeight="140%"
          >
            Total:
          </Text>
          <Text
            color="#000"
            fontSize="3rem"
            fontWeight="300"
            lineHeight="140%"
            fontStyle="normal"
          >
            {totalStations}
          </Text>
        </Flex>
      </Flex>
      <Flex flexDir="column" w="100%">
        <Flex justify="flex-start" w="100%">
          <Text
            color="#2D3748"
            fontSize="1.125rem"
            fontWeight="700"
            lineHeight="140%"
          >
            Ocupadas:
          </Text>
          <Text
            color="#000"
            fontSize="3rem"
            fontWeight="300"
            fontStyle="normal"
            lineHeight="140%"
          >
            {totalActiveStations}
          </Text>
        </Flex>
      </Flex>
      <Flex flexDir="column" w="100%">
        <Flex justify="flex-start" w="100%">
          <Text
            color="#2D3748"
            fontSize="1.125rem"
            fontWeight="700"
            lineHeight="140%"
          >
            Carregadas:
          </Text>
          <Text
            color="#000"
            fontSize="3rem"
            fontWeight="300"
            fontStyle="normal"
            lineHeight="140%"
          >
            {totalChargedStations}
          </Text>
        </Flex>
      </Flex>
    </Flex>
  );
}
