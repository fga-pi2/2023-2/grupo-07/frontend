import { Flex, Heading, Text } from '@chakra-ui/react';

import { useState } from 'react';

import { Drone } from '../../../../types/drone';

interface DashboardDroneCardProps {
  drones: Drone[] | null;
}

export function DashboardDroneCard({ drones }: DashboardDroneCardProps) {
  const totalDrones = drones?.length;
  const totalChargingDrones = drones?.filter(
    (drone) => drone.status === 'charging',
  ).length;

  return (
    <Flex
      flexDir="column"
      borderRadius="1rem"
      bg="white"
      boxShadow="0px 3.5px 5.5px 0px rgba(0, 0, 0, 0.02)"
      gap="1.56rem"
      justifyContent="center"
      padding="2.06rem 3.38rem"
      maxW="420px"
      w="full"
    >
      <Heading
        as="h4"
        textAlign="left"
        color="#2D3748"
        fontSize="1.5rem"
        lineHeight="140%"
      >
        Painel de Drones
      </Heading>
      <svg
        xmlns="http://www.w3.org/2000/svg"
        width="98"
        height="57"
        viewBox="0 0 98 57"
        fill="none"
      >
        <path
          d="M98 4.07143V0H69.4167V4.07143H81.6667V8.14286H77.5833V16.2857H64.1083L57.9793 5.7H40.2412L34.1162 16.2857H20.4167V8.14286H16.3333V4.07143H28.5833V0H0V4.07143H12.25V8.14286H8.16667V28.5H20.4167V24.4286H33.3486L34.2306 25.9513C28.8532 28.6701 24.3352 32.8194 21.1765 37.94C18.0177 43.0605 16.3415 48.9527 16.3333 54.9643C16.3333 55.5042 16.5484 56.022 16.9313 56.4038C17.3142 56.7855 17.8335 57 18.375 57C18.9165 57 19.4358 56.7855 19.8187 56.4038C20.2016 56.022 20.4167 55.5042 20.4167 54.9643C20.4238 49.6672 21.9111 44.4769 24.7117 39.9756C27.5124 35.4744 31.5156 31.8404 36.2723 29.4812L40.2412 36.3334H57.9793L61.8993 29.5626C66.6112 31.9396 70.5708 35.5712 73.3392 40.0549C76.1076 44.5385 77.5766 49.6991 77.5833 54.9643C77.5833 55.5042 77.7984 56.022 78.1813 56.4038C78.5642 56.7855 79.0835 57 79.625 57C80.1665 57 80.6858 56.7855 81.0687 56.4038C81.4516 56.022 81.6667 55.5042 81.6667 54.9643C81.6595 48.9848 80.0023 43.1226 76.8766 38.0196C73.7508 32.9165 69.2769 28.7695 63.945 26.0327L64.8719 24.4286H77.5833V28.5H89.8333V8.14286H85.75V4.07143H98Z"
          fill="#4FD1C5"
        />
      </svg>
      <Flex flexDir="column" w="100%">
        <Flex justify="flex-start" w="100%">
          <Text
            color="#2D3748"
            fontSize="1.125rem"
            fontWeight="700"
            lineHeight="140%"
          >
            Total:
          </Text>
          <Text
            color="#000"
            fontSize="3rem"
            fontWeight="300"
            lineHeight="140%"
            fontStyle="normal"
          >
            {totalDrones}
          </Text>
        </Flex>
      </Flex>
      <Flex flexDir="column" w="100%">
        <Flex justify="flex-start" w="100%">
          <Text
            color="#2D3748"
            fontSize="1.125rem"
            fontWeight="700"
            lineHeight="140%"
          >
            Carregando:
          </Text>
          <Text
            color="#000"
            fontSize="3rem"
            fontWeight="300"
            fontStyle="normal"
            lineHeight="140%"
          >
            {totalChargingDrones}
          </Text>
        </Flex>
      </Flex>
    </Flex>
  );
}
