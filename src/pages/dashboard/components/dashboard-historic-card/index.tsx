import { Flex, Heading, Text, Grid, GridItem } from '@chakra-ui/react';

import { History } from '../../../../types/history';
import { format } from 'date-fns';

interface DashboardHistoricCardProps {
  history: History[] | null;
}

export function DashboardHistoricCard({ history }: DashboardHistoricCardProps) {
  return (
    <Flex
      flexDir="column"
      borderRadius="1rem"
      bg="white"
      boxShadow="0px 3.5px 5.5px 0px rgba(0, 0, 0, 0.02)"
      gap="1.56rem"
      justifyContent="center"
      padding="2.06rem 3.38rem"
      maxW="554px"
      w="full"
      alignItems="center"
    >
      <Flex justify="center">
        <svg
          xmlns="http://www.w3.org/2000/svg"
          width="44"
          height="41"
          viewBox="0 0 44 41"
          fill="none"
        >
          <path
            d="M11.272 33.9349C14.2169 37.0435 18.0919 38.9779 22.2366 39.4086C26.3813 39.8393 30.5392 38.7395 34.0018 36.2968C37.4644 33.8541 40.0174 30.2195 41.2258 26.0125C42.4341 21.8055 42.223 17.2864 40.6283 13.2253C39.0337 9.16415 36.1544 5.81236 32.4809 3.7411C28.8074 1.66984 24.5671 1.00728 20.4826 1.86632C16.3982 2.72536 12.7223 5.05284 10.0815 8.45214C7.44061 11.8514 5.99819 16.1122 6 20.5083V24.7221"
            stroke="#4FD1C5"
            stroke-width="2.5"
            stroke-linecap="round"
            stroke-linejoin="round"
          />
          <path
            d="M2 20.4998L6 24.7221L10 20.4998M22 12.0554V22.6109H32"
            stroke="#4FD1C5"
            stroke-width="2.5"
            stroke-linecap="round"
            stroke-linejoin="round"
          />
        </svg>
      </Flex>
      <Heading
        as="h4"
        textAlign="center"
        color="#2D3748"
        fontSize="1.5rem"
        lineHeight="140%"
      >
        Histórico de carregamento
      </Heading>
      <Flex gap="2rem" w="100%" flexDir="column">
        <Grid
          templateColumns="2fr 2fr 4fr"
          justifyItems="center"
          gap={4}
          w="100%"
          color="#2D3748"
          fontSize="1rem"
          fontWeight="700"
          lineHeight="140%"
          mb="25px"
        >
          <GridItem colSpan={1}>
            {/* Content for the first column */}
            <Text>Base</Text>
          </GridItem>
          <GridItem colSpan={1}>
            {/* Content for the second column */}
            <Text>Drone</Text>
          </GridItem>
          <GridItem colSpan={1}>
            {/* Content for the third column */}
            <Text>Data</Text>
          </GridItem>
        </Grid>
        {history?.map((history: History) => (
          <Grid
            templateColumns="2fr 2fr 4fr"
            gap={4}
            justifyItems="center"
            w="100%"
            color="#718096"
            fontSize="1rem"
            fontWeight="700"
            lineHeight="140%"
            fontStyle="normal"
          >
            <GridItem colSpan={1}>
              {/* Content for the first column */}
              <Text>{history.id_base}</Text>
            </GridItem>
            <GridItem colSpan={1}>
              {/* Content for the second column */}
              <Text>{history.id_drone}</Text>
            </GridItem>
            <GridItem colSpan={1}>
              {/* Content for the third column */}
              <Text>
                {format(new Date(history.loaded_at), 'yyyy-MM-dd HH:mm:ss')}
              </Text>
            </GridItem>
          </Grid>
        ))}
      </Flex>
    </Flex>
  );
}
