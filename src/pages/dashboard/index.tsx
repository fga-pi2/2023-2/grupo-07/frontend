import { Box, Flex, Heading, SimpleGrid } from '@chakra-ui/react';
import { useEffect, useState } from 'react';

import { ImageHeader } from '@components/image-header';

import DashboardHeader from '../../assets/dashboard-header.svg';
import { Sidebar } from '../../components/sidebar';
import { Drone } from '../../types/drone';
import { History } from '../../types/history';
import { Station } from '../../types/station';

import { DashboardDroneCard } from './components/dashboard-drone-card';
import { DashboardHistoricCard } from './components/dashboard-historic-card';
import { DashboardStationCard } from './components/dashboard-stations-card';
import { useGetDroneCard } from './hooks/useGetDroneCard';
import { useGetHistoryCard } from './hooks/useGetHistoryCard';
import { useGetStationCard } from './hooks/useGetStationCard';

export function Dashboard() {
  const [drones, setDrones] = useState<Drone[] | null>(null);
  const [stations, setStations] = useState<Station[] | null>(null);
  const [history, setHistory] = useState<History[] | null>(null);

  const { isLoading: isDronesLoading, handleLoadDrones } = useGetDroneCard({
    setDrones,
  });
  const { isLoading: isStationsLoading, handleLoadStations } =
    useGetStationCard({ setStations });
  const { isLoading: isHistoryLoading, handleLoadHistory } = useGetHistoryCard({
    setHistory,
  });

  useEffect(() => {
    handleLoadDrones();
  }, [handleLoadDrones]);

  useEffect(() => {
    handleLoadStations();
  }, [handleLoadStations]);

  useEffect(() => {
    handleLoadHistory();
  }, [handleLoadHistory]);

  return (
    <Flex gap="2rem">
      <Sidebar />
      <Box as="main" w="full">
        <Flex flexDir="column" alignItems="center">
          <ImageHeader
            pageName="Dashboard"
            image={DashboardHeader}
            imageAlt="Drones"
            width="554px"
            height="449px"
          />

          <Heading
            mt="5.25rem"
            mb="1.5rem"
            color="#2D3748"
            fontSize="1.5rem"
            lineHeight="140%"
            textAlign="left"
            w="full"
          />

          <SimpleGrid columns={3} mt="4rem" gap="1.5" alignItems="start">
            <DashboardDroneCard drones={drones} />
            <DashboardStationCard stations={stations} />
            <DashboardHistoricCard history={history} />
          </SimpleGrid>
        </Flex>
      </Box>
    </Flex>
  );
}
