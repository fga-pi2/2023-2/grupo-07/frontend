import { Dispatch, SetStateAction, useCallback, useState } from 'react';
import { toast } from 'react-toastify';

import { api } from '@config/lib/axios/api';

import { Station } from '../../../types/station';

interface useGetStationsProps {
  setStations: Dispatch<SetStateAction<Station[] | null>>;
}

export function useGetStations({ setStations }: useGetStationsProps) {
  const [isLoading, setIsLoading] = useState(false);

  const handleLoadStations = useCallback(async () => {
    try {
      setIsLoading(true);

      const response = await api.get<Station[]>('/bases/');

      setStations(response.data);
    } catch (err) {
      toast.error(
        'Não foi possível carregar as bases. Tente novamente mais tarde!',
      );
    } finally {
      setIsLoading(false);
    }
  }, [setStations]);

  return { isLoading, handleLoadStations };
}
