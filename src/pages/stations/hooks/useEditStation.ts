import { useState } from 'react';
import { toast } from 'react-toastify';

import { api } from '@config/lib/axios/api';

import { AddStation } from '../components/add-new-station-modal/schema';

interface UseEditStationProps {
  baseId: number;
  onSuccess?: () => void;
}

export function useEditStation(props?: UseEditStationProps) {
  const { onSuccess, baseId } = props ?? {};

  const [isSubmitting, setIsSubmitting] = useState(false);

  async function submit(values: AddStation) {
    setIsSubmitting(true);

    const payload = {
      ...values,
      status: '',
    };

    try {
      await api.put(`/bases/${baseId}`, payload);

      toast.success('Estação atualizada com sucesso!');

      onSuccess?.();
    } catch (err) {
      toast.error('Algo deu errado ao editar a estação. Tente Novamente!');
    } finally {
      setIsSubmitting(false);
    }
  }

  return {
    isSubmitting,
    submit,
  };
}
