import { useState } from 'react';
import { toast } from 'react-toastify';

import { api } from '@config/lib/axios/api';

import { AddStation } from '../components/add-new-station-modal/schema';

interface UseAddNewStationProps {
  onSuccess?: () => void;
}

export function useAddNewStation(props?: UseAddNewStationProps) {
  const { onSuccess } = props ?? {};

  const [isSubmitting, setIsSubmitting] = useState(false);

  async function submit(values: AddStation) {
    setIsSubmitting(true);

    const payload = {
      ...values,
      is_active: true,
      status: '',
    };

    try {
      await api.post('/bases/', payload);

      toast.success('Estação adicionada com sucesso!');

      onSuccess?.();
    } catch (err) {
      toast.error('Algo deu errado ao adicionar a estação. Tente Novamente!');
    } finally {
      setIsSubmitting(false);
    }
  }

  return {
    isSubmitting,
    submit,
  };
}
