import { useState } from 'react';
import { toast } from 'react-toastify';

import { api } from '@config/lib/axios/api';

interface UseDeleteStationProps {
  baseId: number;
  onSuccess?: () => void;
}

export function useDeleteStation(props?: UseDeleteStationProps) {
  const { onSuccess, baseId } = props ?? {};

  const [isDeleting, setIsDeleting] = useState(false);

  async function handleDeleteStation() {
    setIsDeleting(true);

    try {
      await api.delete(`/bases/${baseId}`);

      toast.success('Estação excluída com sucesso!');

      onSuccess?.();
    } catch (err) {
      toast.error('Algo deu errado ao excluír a estação. Tente Novamente!');
    } finally {
      setIsDeleting(false);
    }
  }

  return {
    isDeleting,
    handleDeleteStation,
  };
}
