import {
  Box,
  Button,
  Flex,
  Heading,
  SimpleGrid,
  Spinner,
  Text,
} from '@chakra-ui/react';
import { useEffect, useState } from 'react';

import { ImageHeader } from '@components/image-header';

import StationsHeader from '../../assets/stations-header.svg';
import { Sidebar } from '../../components/sidebar';
import { Station } from '../../types/station';

import { AddNewStationModal } from './components/add-new-station-modal';
import { StationCard } from './components/station-card';
import { useGetStations } from './hooks/useGetStations';

export function Stations() {
  const [stations, setStations] = useState<Station[] | null>(null);
  const [isAddNewStationModalOpen, setIsAddNewStationModalOpen] =
    useState(false);

  const { isLoading, handleLoadStations } = useGetStations({ setStations });

  useEffect(() => {
    handleLoadStations();
  }, [handleLoadStations]);

  if (isLoading) {
    return (
      <Flex gap="2rem">
        <Sidebar />
        <Box as="main" w="full">
          <Flex flexDir="column" alignItems="center">
            <ImageHeader
              image={StationsHeader}
              pageName="Bases"
              imageAlt="Bases dos Drones"
              width="580px"
              height="432px"
            />

            <Heading
              mt="5.25rem"
              mb="1.5rem"
              color="#2D3748"
              fontSize="1.5rem"
              lineHeight="140%"
              textAlign="left"
              w="full"
            >
              Minhas Bases
            </Heading>

            <Spinner size="lg" colorScheme="teal" />
          </Flex>
        </Box>
      </Flex>
    );
  }

  return (
    <>
      <Flex gap="2rem">
        <Sidebar />
        <Box as="main" w="full">
          <Flex flexDir="column" alignItems="center">
            <ImageHeader
              image={StationsHeader}
              pageName="Bases"
              imageAlt="Bases dos Drones"
              width="580px"
              height="432px"
            />

            <Heading
              mt="5.25rem"
              mb="1.5rem"
              color="#2D3748"
              fontSize="1.5rem"
              lineHeight="140%"
              textAlign="left"
              w="full"
            >
              Minhas Bases
            </Heading>

            <SimpleGrid columns={4} spacing={4} w="full">
              {stations ? (
                stations.map((station) => (
                  <StationCard
                    station={station}
                    key={station.id_base}
                    reloadStations={handleLoadStations}
                  />
                ))
              ) : (
                <Text>Nenhuma base cadastrada!</Text>
              )}
            </SimpleGrid>

            <Button
              bg="#4FD1C5"
              _hover={{ bg: '#1d726a' }}
              boxShadow="0px 3.5px 5.5px 0px rgba(0, 0, 0, 0.02)"
              color="white"
              textAlign="center"
              fontSize=".625rem"
              fontWeight="700"
              lineHeight="150%"
              borderRadius=".75rem"
              display="flex"
              alignItems="center"
              justifyContent="center"
              padding="0 .5rem"
              maxW="350px"
              w="full"
              onClick={() => setIsAddNewStationModalOpen(true)}
              mt="2rem"
            >
              ADICIONAR BASE
            </Button>
          </Flex>
        </Box>
      </Flex>

      {isAddNewStationModalOpen ? (
        <AddNewStationModal
          onClose={() => setIsAddNewStationModalOpen(false)}
          reloadStations={handleLoadStations}
        />
      ) : null}
    </>
  );
}
