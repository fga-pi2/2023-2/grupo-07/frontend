import { render, screen } from '@testing-library/react';
import userEvent from '@testing-library/user-event';
import { expect, vi, describe, it } from 'vitest';

import { stationBuilder } from '@tests/builders/station';

import { StationCard } from '.';

describe('StationCard', () => {
  it('should be able to render the card correctly', () => {
    const stationMock = stationBuilder.one();
    const reloadStationsMock = vi.fn();

    render(
      <StationCard station={stationMock} reloadStations={reloadStationsMock} />,
    );

    expect(screen.getByText(stationMock.name)).toBeInTheDocument();
    expect(screen.getByText(stationMock.latitude)).toBeInTheDocument();
    expect(screen.getByText(stationMock.longitude)).toBeInTheDocument();
    expect(screen.getByText(stationMock.id_base)).toBeInTheDocument();
    expect(screen.getByText(stationMock.status)).toBeInTheDocument();
  });

  it('should be able to open edit modal on click ', async () => {
    const stationMock = stationBuilder.one();
    const reloadStationsMock = vi.fn();

    render(
      <StationCard station={stationMock} reloadStations={reloadStationsMock} />,
    );

    await userEvent.click(screen.getByText('EDITAR'));

    expect(screen.getByText('Editar Base')).toBeInTheDocument();
  });
});
