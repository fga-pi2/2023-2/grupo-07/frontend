import {
  Button,
  Flex,
  Heading,
  Modal,
  ModalBody,
  ModalCloseButton,
  ModalContent,
  ModalOverlay,
  Text,
} from '@chakra-ui/react';
import { zodResolver } from '@hookform/resolvers/zod';
import { Controller, useForm } from 'react-hook-form';

import { Input } from '@components/input';
import { useAddNewStation } from '@pages/stations/hooks/useAddNewStation';

import { AddStation, addStationSchema } from './schema';

interface AddNewStationModalProps {
  onClose: () => void;
  reloadStations: () => void;
}

export function AddNewStationModal({
  onClose,
  reloadStations,
}: AddNewStationModalProps) {
  const { isSubmitting, submit } = useAddNewStation({
    onSuccess: () => {
      onClose();
      reloadStations();
    },
  });

  const { control, handleSubmit } = useForm<AddStation>({
    resolver: zodResolver(addStationSchema),
  });

  return (
    <Modal isOpen onClose={onClose} isCentered>
      <ModalOverlay backdropFilter="auto" backdropBlur="4px" />

      <ModalContent
        display="flex"
        alignItems="center"
        justifyContent="center"
        gap="20px"
        flexDir="column"
        padding="56px 72px"
        w="100vw"
        maxW="720px"
      >
        <ModalCloseButton
          bg="#4FD1C5"
          borderRadius="50%"
          color="white"
          fontWeight={700}
          data-testid="close-btn"
        />
        <ModalBody w="full" maxW="350px">
          <Heading
            color="#4FD1C5"
            textAlign="center"
            fontSize="2rem"
            fontWeight="700"
            lineHeight="130%"
          >
            Adicionar Base
          </Heading>

          <Text
            color="#A0AEC0"
            textAlign="center"
            fontSize=".875rem"
            fontWeight="700"
            lineHeight="140%"
            mt=".5rem"
          >
            Adicionar nova base.
          </Text>

          <Flex
            as="form"
            flexDir="column"
            my="1.25rem"
            gap="1rem"
            onSubmit={handleSubmit(submit)}
          >
            <Controller
              name="name"
              control={control}
              defaultValue=""
              render={({
                field: { onChange, value },
                fieldState: { error },
              }) => (
                <Input
                  label="Nome"
                  placeholder="Base Central"
                  onChange={onChange}
                  value={value}
                  error={!!error}
                  errorMessage={error?.message}
                />
              )}
            />

            <Controller
              name="latitude"
              control={control}
              defaultValue=""
              render={({
                field: { onChange, value },
                fieldState: { error },
              }) => (
                <Input
                  label="Latitude"
                  placeholder="-15.98924"
                  onChange={onChange}
                  value={value}
                  error={!!error}
                  errorMessage={error?.message}
                />
              )}
            />

            <Controller
              name="longitude"
              control={control}
              defaultValue=""
              render={({
                field: { onChange, value },
                fieldState: { error },
              }) => (
                <Input
                  label="Longitude"
                  placeholder="-48.044167"
                  onChange={onChange}
                  value={value}
                  error={!!error}
                  errorMessage={error?.message}
                />
              )}
            />

            <Button
              bg="#4FD1C5"
              _hover={{ bg: '#1d726a' }}
              boxShadow="0px 3.5px 5.5px 0px rgba(0, 0, 0, 0.02)"
              color="white"
              textAlign="center"
              fontSize=".625rem"
              fontWeight="700"
              lineHeight="150%"
              borderRadius=".75rem"
              display="flex"
              alignItems="center"
              justifyContent="center"
              padding="0 .5rem"
              maxW="350px"
              w="full"
              mt=".375rem"
              type="submit"
              isLoading={isSubmitting}
              isDisabled={isSubmitting}
            >
              SALVAR
            </Button>
          </Flex>
        </ModalBody>
      </ModalContent>
    </Modal>
  );
}
