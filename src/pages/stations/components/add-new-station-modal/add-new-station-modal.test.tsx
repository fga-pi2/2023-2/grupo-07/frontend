import { render, screen } from '@testing-library/react';
import userEvent from '@testing-library/user-event';
import { expect, vi, describe, it } from 'vitest';

import { mockPostRequest } from '@tests/server';

import { AddNewStationModal } from '.';

describe('AddNewStationModal', () => {
  it('should render the modal correctly', () => {
    const onCloseMock = vi.fn();
    const reloadStationsMock = vi.fn();

    render(
      <AddNewStationModal
        onClose={onCloseMock}
        reloadStations={reloadStationsMock}
      />,
    );

    expect(screen.getByText('Adicionar Base')).toBeInTheDocument();
    expect(screen.getByText('Adicionar nova base.')).toBeInTheDocument();

    expect(screen.getByText('Nome')).toBeInTheDocument();
    expect(screen.getByPlaceholderText('Base Central')).toBeInTheDocument();

    expect(screen.getByText('Latitude')).toBeInTheDocument();
    expect(screen.getByPlaceholderText('40.689247')).toBeInTheDocument();

    expect(screen.getByText('Longitude')).toBeInTheDocument();
    expect(screen.getByPlaceholderText('-74.044502')).toBeInTheDocument();

    expect(screen.getByText('SALVAR')).toBeInTheDocument();
  });

  it('should close the modal on click', async () => {
    const onCloseMock = vi.fn();
    const reloadStationsMock = vi.fn();

    render(
      <AddNewStationModal
        onClose={onCloseMock}
        reloadStations={reloadStationsMock}
      />,
    );

    await userEvent.click(screen.getByTestId('close-btn'));

    expect(onCloseMock).toHaveBeenCalledOnce();
  });

  it('should be able to submit the modal with correct information', async () => {
    const onCloseMock = vi.fn();
    const reloadStationsMock = vi.fn();

    mockPostRequest({
      endpoint: '/bases',
      response: true,
      delay: 0,
      status: 200,
    });

    render(
      <AddNewStationModal
        onClose={onCloseMock}
        reloadStations={reloadStationsMock}
      />,
    );

    const nameInput = screen.getByPlaceholderText('Base Central');

    await userEvent.clear(nameInput);
    await userEvent.type(nameInput, 'Base Teste');

    const latitudeInput = screen.getByPlaceholderText('40.689247');

    await userEvent.clear(latitudeInput);
    await userEvent.type(latitudeInput, '42.123049');

    const longitudeInput = screen.getByPlaceholderText('-74.044502');

    await userEvent.clear(longitudeInput);
    await userEvent.type(longitudeInput, '-31.042752');

    await userEvent.click(screen.getByText('SALVAR'));
  });

  it('should not be able to submit the modal with fields empty', async () => {
    const onCloseMock = vi.fn();
    const reloadStationsMock = vi.fn();

    render(
      <AddNewStationModal
        onClose={onCloseMock}
        reloadStations={reloadStationsMock}
      />,
    );

    await userEvent.click(screen.getByText('SALVAR'));

    expect(screen.getByText('Nome é obrigatório')).toBeInTheDocument();
    expect(screen.getByText('Latitude é obrigatória')).toBeInTheDocument();
    expect(screen.getByText('Longitude é obrigatória')).toBeInTheDocument();
  });
});
