import {
  Button,
  Flex,
  FormControl,
  Heading,
  Modal,
  ModalBody,
  ModalCloseButton,
  ModalContent,
  ModalOverlay,
  Switch,
  Text,
} from '@chakra-ui/react';
import { zodResolver } from '@hookform/resolvers/zod';
import { Controller, useForm } from 'react-hook-form';

import { Input } from '@components/input';
import { useDeleteStation } from '@pages/stations/hooks/useDeleteStation';
import { useEditStation } from '@pages/stations/hooks/useEditStation';

import { Station } from '../../../../types/station';

import { editStationSchema, EditStation } from './schema';

interface EditStationModalProps {
  onClose: () => void;
  station: Station;
  reloadStations: () => void;
}

export function EditStationModal({
  onClose,
  station,
  reloadStations,
}: EditStationModalProps) {
  const { isSubmitting, submit } = useEditStation({
    baseId: station.id_base,
    onSuccess: () => {
      onClose();
      reloadStations();
    },
  });

  const { isDeleting, handleDeleteStation } = useDeleteStation({
    baseId: station.id_base,
    onSuccess: () => {
      onClose();
      reloadStations();
    },
  });

  const { control, handleSubmit } = useForm<EditStation>({
    resolver: zodResolver(editStationSchema),
  });

  return (
    <Modal isOpen onClose={onClose} isCentered>
      <ModalOverlay backdropFilter="auto" backdropBlur="4px" />

      <ModalContent
        display="flex"
        alignItems="center"
        justifyContent="center"
        gap="20px"
        flexDir="column"
        padding="56px 72px"
        w="100vw"
        maxW="720px"
      >
        <ModalCloseButton
          bg="#4FD1C5"
          borderRadius="50%"
          color="white"
          fontWeight={700}
          data-testid="close-btn"
        />
        <ModalBody w="full" maxW="350px">
          <Heading
            color="#4FD1C5"
            textAlign="center"
            fontSize="2rem"
            fontWeight="700"
            lineHeight="130%"
          >
            Editar Base
          </Heading>

          <Text
            color="#A0AEC0"
            textAlign="center"
            fontSize=".875rem"
            fontWeight="700"
            lineHeight="140%"
            mt=".5rem"
          >
            Editar as informações: {station.name}.
          </Text>

          <Flex
            as="form"
            flexDir="column"
            my="1.25rem"
            gap="1rem"
            onSubmit={handleSubmit(submit)}
          >
            <Controller
              name="name"
              control={control}
              defaultValue={station.name}
              render={({
                field: { name, onChange, value },
                fieldState: { error },
              }) => (
                <Input
                  name={name}
                  label="Nome"
                  placeholder="Base Central"
                  onChange={onChange}
                  value={value}
                  error={!!error}
                  errorMessage={error?.message}
                />
              )}
            />

            <Controller
              name="latitude"
              control={control}
              defaultValue={station.latitude}
              render={({
                field: { name, onChange, value },
                fieldState: { error },
              }) => (
                <Input
                  name={name}
                  label="Latitude"
                  placeholder="-15.98924"
                  onChange={onChange}
                  value={value}
                  error={!!error}
                  errorMessage={error?.message}
                />
              )}
            />

            <Controller
              name="longitude"
              control={control}
              defaultValue={station.longitude}
              render={({
                field: { name, onChange, value },
                fieldState: { error },
              }) => (
                <Input
                  name={name}
                  label="Longitude"
                  placeholder="-48.044167"
                  onChange={onChange}
                  value={value}
                  error={!!error}
                  errorMessage={error?.message}
                />
              )}
            />

            <Controller
              name="is_active"
              control={control}
              defaultValue={station.is_active}
              render={({ field: { onChange, value } }) => (
                <FormControl display="flex" alignItems="center" gap=".5rem">
                  <Switch
                    isChecked={value}
                    onChange={onChange}
                    colorScheme="teal"
                  />
                  Ocupada
                </FormControl>
              )}
            />

            <Button
              bg="#4FD1C5"
              _hover={{ bg: '#1d726a' }}
              boxShadow="0px 3.5px 5.5px 0px rgba(0, 0, 0, 0.02)"
              color="white"
              textAlign="center"
              fontSize=".625rem"
              fontWeight="700"
              lineHeight="150%"
              borderRadius=".75rem"
              display="flex"
              alignItems="center"
              justifyContent="center"
              padding="0 .5rem"
              maxW="350px"
              w="full"
              mt=".375rem"
              type="submit"
              isLoading={isSubmitting}
              isDisabled={isSubmitting || isDeleting}
            >
              SALVAR
            </Button>

            <Button
              border="1px solid #4FD1C5"
              _hover={{ bg: '#4FD1C5', color: 'white' }}
              boxShadow="0px 3.5px 5.5px 0px rgba(0, 0, 0, 0.02)"
              color="#4FD1C5"
              textAlign="center"
              fontSize=".625rem"
              fontWeight="700"
              lineHeight="150%"
              borderRadius=".75rem"
              display="flex"
              alignItems="center"
              justifyContent="center"
              padding="0 .5rem"
              maxW="350px"
              w="full"
              mt=".375rem"
              type="button"
              isLoading={isDeleting}
              isDisabled={isSubmitting || isDeleting}
              onClick={() => handleDeleteStation()}
            >
              EXCLUIR
            </Button>
          </Flex>
        </ModalBody>
      </ModalContent>
    </Modal>
  );
}
