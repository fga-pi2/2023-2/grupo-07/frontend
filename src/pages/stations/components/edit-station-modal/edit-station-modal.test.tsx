import { render, screen } from '@testing-library/react';
import userEvent from '@testing-library/user-event';
import { expect, vi, describe, it } from 'vitest';

import { stationBuilder } from '@tests/builders/station';
import { mockPutRequest } from '@tests/server';

import { EditStationModal } from '.';

describe('EditStationModal', () => {
  it('should render the modal correctly', () => {
    const onCloseMock = vi.fn();
    const reloadStationsMock = vi.fn();
    const stationMock = stationBuilder.one();

    render(
      <EditStationModal
        onClose={onCloseMock}
        station={stationMock}
        reloadStations={reloadStationsMock}
      />,
    );

    expect(screen.getByText('Editar Base')).toBeInTheDocument();
    expect(
      screen.getByText(`Editar as informações: ${stationMock.name}.`),
    ).toBeInTheDocument();

    expect(screen.getByText('Nome')).toBeInTheDocument();
    expect(screen.getByLabelText('Nome')).toHaveValue(stationMock.name);

    expect(screen.getByText('Latitude')).toBeInTheDocument();
    expect(screen.getByLabelText('Latitude')).toHaveValue(stationMock.latitude);

    expect(screen.getByText('Longitude')).toBeInTheDocument();
    expect(screen.getByLabelText('Longitude')).toHaveValue(
      stationMock.longitude,
    );

    expect(screen.getByText('Ocupada')).toBeInTheDocument();

    expect(screen.getByText('SALVAR')).toBeInTheDocument();
  });

  it('should close the modal on click', async () => {
    const onCloseMock = vi.fn();
    const reloadStationsMock = vi.fn();
    const stationMock = stationBuilder.one();

    render(
      <EditStationModal
        onClose={onCloseMock}
        station={stationMock}
        reloadStations={reloadStationsMock}
      />,
    );

    await userEvent.click(screen.getByTestId('close-btn'));

    expect(onCloseMock).toHaveBeenCalledOnce();
  });

  it('should be able to submit the modal with correct information', async () => {
    const onCloseMock = vi.fn();
    const reloadStationsMock = vi.fn();
    const stationMock = stationBuilder.one();

    mockPutRequest({
      endpoint: '/bases/:baseId',
      response: true,
      delay: 0,
      status: 200,
    });

    render(
      <EditStationModal
        onClose={onCloseMock}
        station={stationMock}
        reloadStations={reloadStationsMock}
      />,
    );

    const nameInput = screen.getByLabelText('Nome');

    await userEvent.clear(nameInput);
    await userEvent.type(nameInput, 'Base Teste');

    const latitudeInput = screen.getByLabelText('Latitude');

    await userEvent.clear(latitudeInput);
    await userEvent.type(latitudeInput, '42.123049');

    const longitudeInput = screen.getByLabelText('Longitude');

    await userEvent.clear(longitudeInput);
    await userEvent.type(longitudeInput, '-31.042752');

    await userEvent.click(screen.getByText('SALVAR'));
  });

  it('should not be able to submit the modal with fields empty', async () => {
    const onCloseMock = vi.fn();
    const reloadStationsMock = vi.fn();
    const stationMock = stationBuilder.one();

    mockPutRequest({
      endpoint: '/bases/:baseId',
      response: true,
      delay: 0,
      status: 200,
    });

    render(
      <EditStationModal
        onClose={onCloseMock}
        station={stationMock}
        reloadStations={reloadStationsMock}
      />,
    );

    await userEvent.clear(screen.getByLabelText('Nome'));
    await userEvent.clear(screen.getByLabelText('Latitude'));
    await userEvent.clear(screen.getByLabelText('Longitude'));

    await userEvent.click(screen.getByText('SALVAR'));

    expect(screen.getByText('Nome é obrigatório')).toBeInTheDocument();
    expect(screen.getByText('Latitude é obrigatória')).toBeInTheDocument();
    expect(screen.getByText('Longitude é obrigatória')).toBeInTheDocument();
  });
});
