import { z } from 'zod';

export const editStationSchema = z.object({
  name: z
    .string({ required_error: 'Nome é obrigatório' })
    .min(1, 'Nome é obrigatório'),
  latitude: z
    .string({ required_error: 'Latitude é obrigatória' })
    .min(1, 'Latitude é obrigatória'),
  longitude: z
    .string({ required_error: 'Longitude é obrigatória' })
    .min(1, 'Longitude é obrigatória'),
  is_active: z.boolean(),
});

export type EditStation = z.infer<typeof editStationSchema>;
