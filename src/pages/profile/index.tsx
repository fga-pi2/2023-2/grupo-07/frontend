import { Box, Flex, Spinner } from '@chakra-ui/react';
import { useEffect, useState } from 'react';

import { ImageHeader } from '@components/image-header';
import { useGetDrones } from '@pages/drones/hooks/useGetDrones';
import { useGetStations } from '@pages/stations/hooks/useGetStations';

import ProfileHeader from '../../assets/profile-header.svg';
import { Sidebar } from '../../components/sidebar';
import { Drone } from '../../types/drone';
import { Station } from '../../types/station';
import { User } from '../../types/user';

import { DroneCard } from './components/drones-card';
import { ProfileCard } from './components/profile-card';
import { StationsCard } from './components/stations-card';
import { useGetUser } from './hooks/useGetUser';

export function Profile() {
  const [user, setUser] = useState<User | null>(null);
  const [stations, setStations] = useState<Station[] | null>(null);
  const [drones, setDrones] = useState<Drone[] | null>(null);

  const { handleLoadUser, isLoading } = useGetUser({ setUser });
  const { handleLoadStations, isLoading: isLoadingStations } = useGetStations({
    setStations,
  });
  const { handleLoadDrones, isLoading: isLoadingDrones } = useGetDrones({
    setDrones,
  });

  const droneData = {
    total: drones?.length ?? 0,
    charging:
      drones?.filter((drone) => drone.status === 'CARREGANDO').length ?? 0,
  };

  const stationsData = {
    total: drones?.length ?? 0,
    occupied:
      stations?.filter((station) => station.status === 'OCUPADA').length ?? 0,
  };

  useEffect(() => {
    handleLoadUser();
    handleLoadDrones();
    handleLoadStations();
  }, [handleLoadUser, handleLoadStations, handleLoadDrones]);

  return (
    <Flex gap="2rem">
      <Sidebar />
      <Box as="main" w="full">
        <Flex flexDir="column" alignItems="center">
          <ImageHeader
            image={ProfileHeader}
            pageName="Perfil"
            imageAlt="Perfil"
            width="504px"
            height="434px"
          />

          <Flex mt="10rem" w="full">
            {!isLoading && !!user ? (
              <ProfileCard user={user} reloadUserData={handleLoadUser} />
            ) : (
              <Spinner size="lg" colorScheme="teal" />
            )}

            {!isLoadingDrones ? (
              <DroneCard droneData={droneData} />
            ) : (
              <Spinner size="lg" colorScheme="teal" />
            )}

            {!isLoadingStations ? (
              <StationsCard stationsData={stationsData} />
            ) : (
              <Spinner size="lg" colorScheme="teal" />
            )}
          </Flex>
        </Flex>
      </Box>
    </Flex>
  );
}
