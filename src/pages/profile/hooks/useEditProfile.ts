import { useState } from 'react';
import { toast } from 'react-toastify';

import { api } from '@config/lib/axios/api';

import { EditProfile } from '../components/edit-profile-modal/schema';

interface UseEditProfileProps {
  onSuccess?: () => void;
}

export function useEditProfile(props?: UseEditProfileProps) {
  const { onSuccess } = props ?? {};

  const [isSubmitting, setIsSubmitting] = useState(false);

  async function submit({ username, phone, password }: EditProfile) {
    setIsSubmitting(true);

    try {
      await api.put('/users/', { username, phone, password });

      toast.success('Usuário atualizado com sucesso!');

      onSuccess?.();
    } catch (err) {
      toast.error('Algo deu errado ao editar seus dados. Tente Novamente!');
    } finally {
      setIsSubmitting(false);
    }
  }

  return {
    isSubmitting,
    submit,
  };
}
