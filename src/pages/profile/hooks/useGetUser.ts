import { Dispatch, SetStateAction, useCallback, useState } from 'react';
import { toast } from 'react-toastify';

import { api } from '@config/lib/axios/api';

import { User } from '../../../types/user';

interface UseGetUserProps {
  setUser: Dispatch<SetStateAction<User | null>>;
}

export function useGetUser({ setUser }: UseGetUserProps) {
  const [isLoading, setIsLoading] = useState(false);

  const handleLoadUser = useCallback(async () => {
    try {
      setIsLoading(true);

      const response = await api.get<User>('/users/me');

      setUser(response.data);
    } catch (err) {
      toast.error(
        'Não foi possível carregar os drones. Tente novamente mais tarde!',
      );
    } finally {
      setIsLoading(false);
    }
  }, [setUser]);

  return { isLoading, handleLoadUser };
}
