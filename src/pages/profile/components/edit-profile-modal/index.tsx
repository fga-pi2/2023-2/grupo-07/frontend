import {
  Button,
  Flex,
  FormControl,
  FormLabel,
  Heading,
  Modal,
  ModalBody,
  ModalCloseButton,
  ModalContent,
  ModalOverlay,
  Text,
  Input,
} from '@chakra-ui/react';
import { zodResolver } from '@hookform/resolvers/zod';
import { useForm } from 'react-hook-form';

import { useEditProfile } from '@pages/profile/hooks/useEditProfile';

import { User } from '../../../../types/user';

import { EditProfile, editProfileSchema } from './schema';

interface EditProfileModalProps {
  onClose: () => void;
  user: User;
  reloadUserData: () => void;
}

export function EditProfileModal({
  onClose,
  reloadUserData,
  user,
}: EditProfileModalProps) {
  const { isSubmitting, submit } = useEditProfile({
    onSuccess: () => {
      onClose();
      reloadUserData();
    },
  });

  const { register, handleSubmit } = useForm<EditProfile>({
    resolver: zodResolver(editProfileSchema),
    defaultValues: {
      email: user?.email ?? '',
      password: '',
      phone: user?.phone ?? '',
      repeatPassword: '',
      username: user?.username ?? '',
    },
  });

  return (
    <Modal isOpen onClose={onClose} isCentered>
      <ModalOverlay backdropFilter="auto" backdropBlur="4px" />

      <ModalContent
        display="flex"
        alignItems="center"
        justifyContent="center"
        gap="20px"
        flexDir="column"
        padding="56px 72px"
        w="100vw"
        maxW="720px"
      >
        <ModalCloseButton
          bg="#4FD1C5"
          borderRadius="50%"
          color="white"
          fontWeight={700}
          data-testid="close-btn"
        />
        <ModalBody w="full" maxW="350px">
          <Heading
            color="#4FD1C5"
            textAlign="center"
            fontSize="2rem"
            fontWeight="700"
            lineHeight="130%"
          >
            Editar Usuário
          </Heading>

          <Text
            color="#A0AEC0"
            textAlign="center"
            fontSize=".875rem"
            fontWeight="700"
            lineHeight="140%"
            mt=".5rem"
          >
            Editar as informações do seu usuário.
          </Text>

          <Flex
            as="form"
            flexDir="column"
            my="1.25rem"
            gap="1rem"
            onSubmit={handleSubmit(submit)}
          >
            <FormControl>
              <FormLabel
                width="271px"
                height="19px"
                flexShrink={0}
                color="var(--gray-gray-700, #2D3748)"
                fontSize="14px"
                fontStyle="normal"
                fontWeight="400"
                lineHeight="140%" /* 19.6px */
              >
                Username
              </FormLabel>
              <Input
                type="text"
                placeholder="Username"
                display="flex"
                width="350px"
                height="50px"
                padding="0px 20px"
                alignItems="center"
                flexShrink={0}
                borderRadius="15px"
                border="1px solid var(--gray-gray-200, #E2E8F0)"
                background="var(--black-amp-white-white, #FFF)"
                {...register('username', {
                  required: 'Campo obrigatório',
                })}
              />
            </FormControl>

            <FormControl>
              <FormLabel
                width="271px"
                height="19px"
                flexShrink={0}
                color="var(--gray-gray-700, #2D3748)"
                fontSize="14px"
                fontStyle="normal"
                fontWeight="400"
                lineHeight="140%" /* 19.6px */
                marginTop="5px"
              >
                Email
              </FormLabel>
              <Input
                type="email"
                placeholder="Email"
                display="flex"
                width="350px"
                height="50px"
                padding="0px 20px"
                alignItems="center"
                readOnly
                flexShrink={0}
                borderRadius="15px"
                border="1px solid var(--gray-gray-200, #E2E8F0)"
                background="var(--black-amp-white-white, #FFF)"
                {...register('email', {
                  required: 'Campo obrigatório',
                })}
              />
            </FormControl>

            <FormControl>
              <FormLabel
                width="271px"
                height="19px"
                flexShrink={0}
                color="var(--gray-gray-700, #2D3748)"
                fontSize="14px"
                fontStyle="normal"
                fontWeight="400"
                lineHeight="140%" /* 19.6px */
                marginTop="5px"
              >
                Telefone
              </FormLabel>
              <Input
                type="phone"
                placeholder="Telefone"
                display="flex"
                width="350px"
                height="50px"
                padding="0px 20px"
                alignItems="center"
                flexShrink={0}
                borderRadius="15px"
                border="1px solid var(--gray-gray-200, #E2E8F0)"
                background="var(--black-amp-white-white, #FFF)"
                {...register('phone', {
                  required: 'Campo obrigatório',
                })}
              />
            </FormControl>

            <FormControl>
              <FormLabel
                width="271px"
                height="19px"
                flexShrink={0}
                color="var(--gray-gray-700, #2D3748)"
                fontSize="14px"
                fontStyle="normal"
                fontWeight="400"
                lineHeight="140%" /* 19.6px */
                marginTop="5px"
              >
                Senha
              </FormLabel>
              <Input
                type="password"
                placeholder="Senha"
                display="flex"
                width="350px"
                height="50px"
                padding="0px 20px"
                alignItems="center"
                flexShrink={0}
                borderRadius="15px"
                border="1px solid var(--gray-gray-200, #E2E8F0)"
                background="var(--black-amp-white-white, #FFF)"
                {...register('password', {
                  required: 'Campo obrigatório',
                })}
              />
            </FormControl>

            <FormControl>
              <FormLabel
                width="271px"
                height="19px"
                flexShrink={0}
                color="var(--gray-gray-700, #2D3748)"
                fontSize="14px"
                fontStyle="normal"
                fontWeight="400"
                lineHeight="140%" /* 19.6px */
                marginTop="5px"
              >
                Repita a Senha
              </FormLabel>
              <Input
                type="password"
                placeholder="Repita sua senha"
                display="flex"
                width="350px"
                height="50px"
                padding="0px 20px"
                alignItems="center"
                flexShrink={0}
                borderRadius="15px"
                border="1px solid var(--gray-gray-200, #E2E8F0)"
                background="var(--black-amp-white-white, #FFF)"
                {...register('repeatPassword', {
                  required: 'Campo obrigatório',
                })}
              />
            </FormControl>

            <Button
              bg="#4FD1C5"
              _hover={{ bg: '#1d726a' }}
              boxShadow="0px 3.5px 5.5px 0px rgba(0, 0, 0, 0.02)"
              color="white"
              textAlign="center"
              fontSize=".625rem"
              fontWeight="700"
              lineHeight="150%"
              borderRadius=".75rem"
              display="flex"
              alignItems="center"
              justifyContent="center"
              padding="0 .5rem"
              maxW="350px"
              w="full"
              mt=".375rem"
              type="submit"
              isLoading={isSubmitting}
              isDisabled={isSubmitting}
            >
              SALVAR
            </Button>
          </Flex>
        </ModalBody>
      </ModalContent>
    </Modal>
  );
}
