import { Button, Flex, Heading, Text } from '@chakra-ui/react';
import { useNavigate } from 'react-router-dom';

import { ROUTER_PATHS } from '@router/index';

interface StationsData {
  total: number;
  occupied: number;
}

interface StationsCardProps {
  stationsData: StationsData;
}

export function StationsCard({ stationsData }: StationsCardProps) {
  const { total, occupied } = stationsData ?? {};
  const navigate = useNavigate();

  return (
    <Flex
      flexDir="column"
      borderRadius="1rem"
      bg="white"
      boxShadow="0px 3.5px 5.5px 0px rgba(0, 0, 0, 0.02)"
      gap=".625rem"
      justifyContent="center"
      padding="1.25rem 3.125rem"
      maxW="389px"
      w="full"
    >
      <Heading
        as="h4"
        textAlign="left"
        color="#2D3748"
        fontSize="1rem"
        lineHeight="140%"
      >
        Bases
      </Heading>
      <Text
        color="#A0AEC0"
        fontSize=".75rem"
        fontWeight="400"
        lineHeight="150%"
      >
        Suas bases cadastradas
      </Text>
      <Text
        color="#A0AEC0"
        fontSize=".75rem"
        fontWeight="400"
        lineHeight="150%"
      >
        <Text as="strong" color="#718096" fontWeight="700">
          Total:{' '}
        </Text>
        {total}
      </Text>

      <Text
        color="#A0AEC0"
        fontSize=".75rem"
        fontWeight="400"
        lineHeight="150%"
      >
        <Text as="strong" color="#718096" fontWeight="700">
          Ocupadas:{' '}
        </Text>
        {occupied}
      </Text>

      <Button
        display="flex"
        alignItems="center"
        justifyContent="center"
        padding="0 .5rem"
        height="28px"
        maxW="187px"
        w="full"
        bg="#4FD1C5"
        borderRadius=".75rem"
        margin="0 auto"
        _hover={{ bg: '#1d726a' }}
        color="white"
        textAlign="center"
        fontSize="10px"
        fontWeight="700"
        lineHeight="150%"
        mt=".25rem"
        onClick={() => navigate(ROUTER_PATHS.STATIONS)}
      >
        VER TODOS
      </Button>
    </Flex>
  );
}
