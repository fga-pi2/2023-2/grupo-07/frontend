import { Button, Flex, Heading, Text } from '@chakra-ui/react';
import { useState } from 'react';

import { User } from '../../../../types/user';
import { EditProfileModal } from '../edit-profile-modal';

interface ProfieCardProps {
  user: User;
  reloadUserData: () => void;
}

export function ProfileCard({ user, reloadUserData }: ProfieCardProps) {
  const { email, phone, username } = user ?? {};

  const [isEditProfileModalOpen, setIsEditProfileModalOpen] = useState(false);

  return (
    <>
      <Flex
        flexDir="column"
        borderRadius="1rem"
        bg="white"
        boxShadow="0px 3.5px 5.5px 0px rgba(0, 0, 0, 0.02)"
        gap=".625rem"
        justifyContent="center"
        padding="1.25rem 3.125rem"
        maxW="389px"
        w="full"
      >
        <Heading
          as="h4"
          textAlign="center"
          color="#2D3748"
          fontSize="1.125rem"
          lineHeight="140%"
        >
          Informação do perfil
        </Heading>
        <Text
          color="#A0AEC0"
          fontSize=".75rem"
          fontWeight="400"
          lineHeight="150%"
        >
          <Text as="strong" color="#718096" fontWeight="700">
            Username:{' '}
          </Text>
          {username}
        </Text>

        <Text
          color="#A0AEC0"
          fontSize=".75rem"
          fontWeight="400"
          lineHeight="150%"
        >
          <Text as="strong" color="#718096" fontWeight="700">
            Telefone:{' '}
          </Text>
          {phone}
        </Text>

        <Text
          color="#A0AEC0"
          fontSize=".75rem"
          fontWeight="400"
          lineHeight="150%"
        >
          <Text as="strong" color="#718096" fontWeight="700">
            Email:{' '}
          </Text>
          {email}
        </Text>

        <Button
          display="flex"
          alignItems="center"
          justifyContent="center"
          padding="0 .5rem"
          height="28px"
          maxW="187px"
          w="full"
          bg="#4FD1C5"
          borderRadius=".75rem"
          margin="0 auto"
          _hover={{ bg: '#1d726a' }}
          color="white"
          textAlign="center"
          fontSize="10px"
          fontWeight="700"
          lineHeight="150%"
          mt=".25rem"
          onClick={() => setIsEditProfileModalOpen(true)}
        >
          EDITAR
        </Button>
      </Flex>

      {isEditProfileModalOpen ? (
        <EditProfileModal
          onClose={() => setIsEditProfileModalOpen(false)}
          user={user}
          reloadUserData={reloadUserData}
        />
      ) : null}
    </>
  );
}
