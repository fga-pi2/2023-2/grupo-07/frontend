import {
  Box,
  Button,
  Flex,
  Heading,
  SimpleGrid,
  Spinner,
  Text,
} from '@chakra-ui/react';
import { useEffect, useState } from 'react';

import { ImageHeader } from '@components/image-header';
import { useGetStations } from '@pages/stations/hooks/useGetStations';

import DronesHeader from '../../assets/drone-header.svg';
import { Sidebar } from '../../components/sidebar';
import { Drone } from '../../types/drone';
import { Station } from '../../types/station';

import { AddNewDroneModal } from './components/add-new-drone-modal';
import { DroneCard } from './components/drone-card';
import { useGetDrones } from './hooks/useGetDrones';

export function Drones() {
  const [drones, setDrones] = useState<Drone[] | null>(null);
  const [stations, setStations] = useState<Station[] | null>(null);
  const [isAddNewDroneModalOpen, setIsAddNewDroneModalOpen] = useState(false);

  const { isLoading, handleLoadDrones } = useGetDrones({ setDrones });
  const { handleLoadStations } = useGetStations({
    setStations,
  });

  useEffect(() => {
    handleLoadDrones();
    handleLoadStations();
  }, [handleLoadDrones, handleLoadStations]);

  if (isLoading) {
    return (
      <Flex gap="2rem">
        <Sidebar />
        <Box as="main" w="full">
          <Flex flexDir="column" alignItems="center">
            <ImageHeader
              image={DronesHeader}
              pageName="Drones"
              imageAlt="Drones"
              width="504px"
              height="434px"
            />

            <Heading
              mt="5.25rem"
              mb="1.5rem"
              color="#2D3748"
              fontSize="1.5rem"
              lineHeight="140%"
              textAlign="left"
              w="full"
            >
              Meus drones
            </Heading>

            <Spinner size="lg" colorScheme="teal" />
          </Flex>
        </Box>
      </Flex>
    );
  }

  return (
    <>
      <Flex gap="2rem">
        <Sidebar />
        <Box as="main" w="full">
          <Flex flexDir="column" alignItems="center">
            <ImageHeader
              image={DronesHeader}
              pageName="Drones"
              imageAlt="Drones"
              width="504px"
              height="434px"
            />

            <Heading
              mt="5.25rem"
              mb="1.5rem"
              color="#2D3748"
              fontSize="1.5rem"
              lineHeight="140%"
              textAlign="left"
              w="full"
            >
              Meus drones
            </Heading>

            <SimpleGrid columns={4} spacing={4} w="full">
              {drones ? (
                drones.map((drone) => (
                  <DroneCard
                    drone={drone}
                    key={drone.id_drone}
                    reloadDrones={handleLoadDrones}
                  />
                ))
              ) : (
                <Text>Nenhum drone cadastrado!</Text>
              )}
            </SimpleGrid>

            <Button
              bg="#4FD1C5"
              _hover={{ bg: '#1d726a' }}
              boxShadow="0px 3.5px 5.5px 0px rgba(0, 0, 0, 0.02)"
              color="white"
              textAlign="center"
              fontSize=".625rem"
              fontWeight="700"
              lineHeight="150%"
              borderRadius=".75rem"
              display="flex"
              alignItems="center"
              justifyContent="center"
              padding="0 .5rem"
              maxW="350px"
              w="full"
              onClick={() => setIsAddNewDroneModalOpen(true)}
              mt="2rem"
            >
              ADICIONAR DRONE
            </Button>
          </Flex>
        </Box>
      </Flex>

      {isAddNewDroneModalOpen ? (
        <AddNewDroneModal
          onClose={() => setIsAddNewDroneModalOpen(false)}
          reloadDrones={handleLoadDrones}
          stations={stations || []}
        />
      ) : null}
    </>
  );
}
