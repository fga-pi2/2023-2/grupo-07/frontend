import { useState } from 'react';
import { toast } from 'react-toastify';

import { api } from '@config/lib/axios/api';

import { EditDrone } from '../components/edit-drone-modal/schema';

interface UseEditDroneProps {
  droneId: number;
  onSuccess?: () => void;
}

export function useEditDrone(props?: UseEditDroneProps) {
  const { onSuccess, droneId } = props ?? {};

  const [isSubmitting, setIsSubmitting] = useState(false);

  async function submit(values: EditDrone) {
    setIsSubmitting(true);

    try {
      await api.put(`/drones/${droneId}`, values);

      toast.success('Drone atualizado com sucesso!');

      onSuccess?.();
    } catch (err) {
      toast.error('Algo deu errado ao editar o drone. Tente Novamente!');
    } finally {
      setIsSubmitting(false);
    }
  }

  return {
    isSubmitting,
    submit,
  };
}
