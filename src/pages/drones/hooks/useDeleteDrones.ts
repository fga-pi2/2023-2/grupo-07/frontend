import { useState } from 'react';
import { toast } from 'react-toastify';

import { api } from '@config/lib/axios/api';

interface UseDeleteDronesProps {
  droneId: number;
  onSuccess?: () => void;
}

export function useDeleteDrones(props?: UseDeleteDronesProps) {
  const { onSuccess, droneId } = props ?? {};

  const [isDeleting, setIsDeleting] = useState(false);

  async function handleDeleteDrones() {
    setIsDeleting(true);

    try {
      await api.delete(`/drones/${droneId}`);

      toast.success('Drone excluído com sucesso!');

      onSuccess?.();
    } catch (err) {
      toast.error('Algo deu errado ao excluír o drone. Tente Novamente!');
    } finally {
      setIsDeleting(false);
    }
  }

  return {
    isDeleting,
    handleDeleteDrones,
  };
}
