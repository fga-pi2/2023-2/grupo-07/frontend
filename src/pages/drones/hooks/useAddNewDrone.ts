import { useState } from 'react';
import { toast } from 'react-toastify';

import { api } from '@config/lib/axios/api';

import { AddDrone } from '../components/add-new-drone-modal/schema';

interface UseAddNewDroneProps {
  onSuccess?: () => void;
}

export function useAddNewDrone(props?: UseAddNewDroneProps) {
  const { onSuccess } = props ?? {};

  const [isSubmitting, setIsSubmitting] = useState(false);

  async function submit(values: AddDrone) {
    setIsSubmitting(true);
    try {
      const obj = {
        ...values,
        status: 'Não Carregando',
      };
      await api.post('/drones/', obj);

      toast.success('Drone adicionado com sucesso!');

      onSuccess?.();
    } catch (err) {
      toast.error('Algo deu errado ao adicionar o drone. Tente Novamente!');
    } finally {
      setIsSubmitting(false);
    }
  }

  return {
    isSubmitting,
    submit,
  };
}
