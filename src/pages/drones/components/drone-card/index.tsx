import { Button, Flex, Heading, Text } from '@chakra-ui/react';
import { useState } from 'react';

import { Drone } from '../../../../types/drone';
import { EditDroneModal } from '../edit-drone-modal';

interface StationCardProps {
  drone: Drone;
  reloadDrones: () => void;
}

export function DroneCard({ drone, reloadDrones }: StationCardProps) {
  const { serie, id_drone, status } = drone ?? {};

  const [isEditDroneModalOpen, setIsEditDroneModalOpen] = useState(false);

  const parsedStatus = status === 'CARREGANDO' ? 'Carregando' : ' Em uso';

  return (
    <>
      <Flex
        flexDir="column"
        borderRadius="1rem"
        bg="white"
        boxShadow="0px 3.5px 5.5px 0px rgba(0, 0, 0, 0.02)"
        gap=".625rem"
        justifyContent="center"
        padding="1.25rem 3.125rem"
        maxW="389px"
        w="full"
      >
        <Heading
          as="h4"
          textAlign="center"
          color="#2D3748"
          fontSize="1.125rem"
          lineHeight="140%"
        >
          {serie}
        </Heading>
        <Text
          color="#A0AEC0"
          fontSize=".75rem"
          fontWeight="400"
          lineHeight="150%"
        >
          <Text as="strong" color="#718096" fontWeight="700">
            ID:{' '}
          </Text>
          {id_drone}
        </Text>

        <Text
          color="#A0AEC0"
          fontSize=".75rem"
          fontWeight="400"
          lineHeight="150%"
        >
          <Text as="strong" color="#718096" fontWeight="700">
            Status:{' '}
          </Text>
          {parsedStatus}
        </Text>

        <Button
          display="flex"
          alignItems="center"
          justifyContent="center"
          padding="0 .5rem"
          height="28px"
          maxW="187px"
          w="full"
          bg="#4FD1C5"
          borderRadius=".75rem"
          margin="0 auto"
          _hover={{ bg: '#1d726a' }}
          color="white"
          textAlign="center"
          fontSize="10px"
          fontWeight="700"
          lineHeight="150%"
          mt=".25rem"
          onClick={() => setIsEditDroneModalOpen(true)}
        >
          EDITAR
        </Button>
      </Flex>

      {isEditDroneModalOpen ? (
        <EditDroneModal
          onClose={() => setIsEditDroneModalOpen(false)}
          drone={drone}
          reloadDrones={reloadDrones}
        />
      ) : null}
    </>
  );
}
