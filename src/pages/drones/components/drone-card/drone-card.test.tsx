import { render, screen } from '@testing-library/react';
import userEvent from '@testing-library/user-event';
import { expect, vi, describe, it } from 'vitest';

import { droneBuilder } from '@tests/builders/drone';

import { DroneCard } from '.';

describe('DroneCard', () => {
  it('should be able to render the card correctly', () => {
    const droneMock = droneBuilder.one();
    const reloadDronesMock = vi.fn();

    render(<DroneCard drone={droneMock} reloadDrones={reloadDronesMock} />);

    expect(screen.getByText(droneMock.serie)).toBeInTheDocument();
    expect(screen.getByText(droneMock.id_drone)).toBeInTheDocument();
    expect(screen.getByText(droneMock.status)).toBeInTheDocument();
  });

  it('should be able to open edit modal on click ', async () => {
    const droneMock = droneBuilder.one();
    const reloadDronesMock = vi.fn();

    render(<DroneCard drone={droneMock} reloadDrones={reloadDronesMock} />);

    await userEvent.click(screen.getByText('EDITAR'));

    expect(screen.getByText('Editar Drone')).toBeInTheDocument();
  });
});
