import {
  Button,
  Flex,
  Heading,
  Modal,
  ModalBody,
  ModalCloseButton,
  ModalContent,
  ModalOverlay,
  Text,
} from '@chakra-ui/react';
import { zodResolver } from '@hookform/resolvers/zod';
import { Controller, useForm } from 'react-hook-form';

import { Input } from '@components/input';
import { useDeleteDrones } from '@pages/drones/hooks/useDeleteDrones';
import { useEditDrone } from '@pages/drones/hooks/useEditDrone';

import { Drone } from '../../../../types/drone';

import { EditDrone, editDroneSchema } from './schema';

interface EditDroneModalProps {
  onClose: () => void;
  drone: Drone;
  reloadDrones: () => void;
}

export function EditDroneModal({
  onClose,
  drone,
  reloadDrones,
}: EditDroneModalProps) {
  const { isSubmitting, submit } = useEditDrone({
    droneId: drone.id_drone,
    onSuccess: () => {
      onClose();
      reloadDrones();
    },
  });

  const { handleDeleteDrones, isDeleting } = useDeleteDrones({
    droneId: drone.id_drone,
    onSuccess: () => {
      onClose();
      reloadDrones();
    },
  });

  const { control, handleSubmit } = useForm<EditDrone>({
    resolver: zodResolver(editDroneSchema),
  });

  return (
    <Modal isOpen onClose={onClose} isCentered>
      <ModalOverlay backdropFilter="auto" backdropBlur="4px" />

      <ModalContent
        display="flex"
        alignItems="center"
        justifyContent="center"
        gap="20px"
        flexDir="column"
        padding="56px 72px"
        w="100vw"
        maxW="720px"
      >
        <ModalCloseButton
          bg="#4FD1C5"
          borderRadius="50%"
          color="white"
          fontWeight={700}
          data-testid="close-btn"
        />
        <ModalBody w="full" maxW="350px">
          <Heading
            color="#4FD1C5"
            textAlign="center"
            fontSize="2rem"
            fontWeight="700"
            lineHeight="130%"
          >
            Editar Drone
          </Heading>

          <Text
            color="#A0AEC0"
            textAlign="center"
            fontSize=".875rem"
            fontWeight="700"
            lineHeight="140%"
            mt=".5rem"
          >
            Editar as informações do drone {drone.serie}.
          </Text>

          <Flex
            as="form"
            flexDir="column"
            my="1.25rem"
            gap="1rem"
            onSubmit={handleSubmit(submit)}
          >
            <Controller
              name="serie"
              control={control}
              defaultValue={drone.serie}
              render={({
                field: { name, onChange, value },
                fieldState: { error },
              }) => (
                <Input
                  name={name}
                  label="RFID"
                  placeholder="30322073666942036461"
                  onChange={onChange}
                  value={value}
                  error={!!error}
                  errorMessage={error?.message}
                />
              )}
            />

            <Button
              bg="#4FD1C5"
              _hover={{ bg: '#1d726a' }}
              boxShadow="0px 3.5px 5.5px 0px rgba(0, 0, 0, 0.02)"
              color="white"
              textAlign="center"
              fontSize=".625rem"
              fontWeight="700"
              lineHeight="150%"
              borderRadius=".75rem"
              display="flex"
              alignItems="center"
              justifyContent="center"
              padding="0 .5rem"
              maxW="350px"
              w="full"
              mt=".375rem"
              type="submit"
              isLoading={isSubmitting}
              isDisabled={isSubmitting || isDeleting}
            >
              SALVAR
            </Button>

            <Button
              border="1px solid #4FD1C5"
              _hover={{ bg: '#4FD1C5', color: 'white' }}
              boxShadow="0px 3.5px 5.5px 0px rgba(0, 0, 0, 0.02)"
              color="#4FD1C5"
              textAlign="center"
              fontSize=".625rem"
              fontWeight="700"
              lineHeight="150%"
              borderRadius=".75rem"
              display="flex"
              alignItems="center"
              justifyContent="center"
              padding="0 .5rem"
              maxW="350px"
              w="full"
              mt=".375rem"
              type="button"
              isLoading={isDeleting}
              isDisabled={isSubmitting || isDeleting}
              onClick={() => handleDeleteDrones()}
            >
              EXCLUIR
            </Button>
          </Flex>
        </ModalBody>
      </ModalContent>
    </Modal>
  );
}
