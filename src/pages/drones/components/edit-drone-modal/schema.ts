import { z } from 'zod';

export const editDroneSchema = z.object({
  serie: z
    .string({ required_error: 'RFID é obrigatório' })
    .min(1, 'RFID é obrigatório'),
});

export type EditDrone = z.infer<typeof editDroneSchema>;
