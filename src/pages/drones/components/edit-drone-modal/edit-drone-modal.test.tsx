import { render, screen } from '@testing-library/react';
import userEvent from '@testing-library/user-event';
import { expect, vi, describe, it } from 'vitest';

import { droneBuilder } from '@tests/builders/drone';
import { mockPutRequest } from '@tests/server';

import { EditDroneModal } from '.';

describe('EditDroneModal', () => {
  it('should render the modal correctly', () => {
    const onCloseMock = vi.fn();
    const reloadStationsMock = vi.fn();
    const droneMock = droneBuilder.one();

    render(
      <EditDroneModal
        onClose={onCloseMock}
        drone={droneMock}
        reloadDrones={reloadStationsMock}
      />,
    );

    expect(screen.getByText('Editar Drone')).toBeInTheDocument();
    expect(
      screen.getByText(`Editar as informações do drone ${droneMock.serie}.`),
    ).toBeInTheDocument();

    expect(screen.getByText('RFID')).toBeInTheDocument();
    expect(screen.getByLabelText('RFID')).toHaveValue(droneMock.serie);

    expect(screen.getByText('SALVAR')).toBeInTheDocument();
  });

  it('should close the modal on click', async () => {
    const onCloseMock = vi.fn();
    const reloadStationsMock = vi.fn();
    const droneMock = droneBuilder.one();

    render(
      <EditDroneModal
        onClose={onCloseMock}
        drone={droneMock}
        reloadDrones={reloadStationsMock}
      />,
    );

    await userEvent.click(screen.getByTestId('close-btn'));

    expect(onCloseMock).toHaveBeenCalledOnce();
  });

  it('should be able to submit the modal with correct information', async () => {
    const onCloseMock = vi.fn();
    const reloadStationsMock = vi.fn();
    const droneMock = droneBuilder.one();

    mockPutRequest({
      endpoint: '/drones/:droneId',
      response: true,
      delay: 0,
      status: 200,
    });

    render(
      <EditDroneModal
        onClose={onCloseMock}
        drone={droneMock}
        reloadDrones={reloadStationsMock}
      />,
    );

    const serieInput = screen.getByLabelText('RFID');

    await userEvent.clear(serieInput);
    await userEvent.type(serieInput, 'MOCK DE SERIE');

    await userEvent.click(screen.getByText('SALVAR'));
  });

  it('should not be able to submit the modal with fields empty', async () => {
    const onCloseMock = vi.fn();
    const reloadStationsMock = vi.fn();
    const droneMock = droneBuilder.one();

    mockPutRequest({
      endpoint: '/drones/:droneId',
      response: true,
      delay: 0,
      status: 200,
    });

    render(
      <EditDroneModal
        onClose={onCloseMock}
        drone={droneMock}
        reloadDrones={reloadStationsMock}
      />,
    );

    await userEvent.clear(screen.getByLabelText('RFID'));

    await userEvent.click(screen.getByText('SALVAR'));

    expect(
      screen.getByText('Número de série é obrigatório'),
    ).toBeInTheDocument();
  });
});
