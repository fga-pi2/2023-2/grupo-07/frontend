import { render, screen } from '@testing-library/react';
import userEvent from '@testing-library/user-event';
import { expect, vi, describe, it } from 'vitest';

import { mockPostRequest } from '@tests/server';

import { AddNewDroneModal } from '.';

describe('AddNewDroneModal', () => {
  it('should render the modal correctly', () => {
    const onCloseMock = vi.fn();
    const reloadStationsMock = vi.fn();

    render(
      <AddNewDroneModal
        onClose={onCloseMock}
        reloadDrones={reloadStationsMock}
      />,
    );

    expect(screen.getByText('Adicionar Drone')).toBeInTheDocument();
    expect(screen.getByText('Adicionar novo drone.')).toBeInTheDocument();

    expect(screen.getByText('RFID')).toBeInTheDocument();
    expect(
      screen.getByPlaceholderText('30322073666942036461'),
    ).toBeInTheDocument();

    expect(screen.getByText('SALVAR')).toBeInTheDocument();
  });

  it('should close the modal on click', async () => {
    const onCloseMock = vi.fn();
    const reloadStationsMock = vi.fn();

    render(
      <AddNewDroneModal
        onClose={onCloseMock}
        reloadDrones={reloadStationsMock}
      />,
    );

    await userEvent.click(screen.getByTestId('close-btn'));

    expect(onCloseMock).toHaveBeenCalledOnce();
  });

  it('should be able to submit the modal with correct information', async () => {
    const onCloseMock = vi.fn();
    const reloadStationsMock = vi.fn();

    mockPostRequest({
      endpoint: '/drones',
      response: true,
      delay: 0,
      status: 200,
    });

    render(
      <AddNewDroneModal
        onClose={onCloseMock}
        reloadDrones={reloadStationsMock}
      />,
    );

    const serieInput = screen.getByLabelText('RFID');

    await userEvent.clear(serieInput);
    await userEvent.type(serieInput, 'MOCK SERIE');

    await userEvent.click(screen.getByText('SALVAR'));
  });

  it('should not be able to submit the modal with fields empty', async () => {
    const onCloseMock = vi.fn();
    const reloadStationsMock = vi.fn();

    render(
      <AddNewDroneModal
        onClose={onCloseMock}
        reloadDrones={reloadStationsMock}
      />,
    );

    await userEvent.clear(screen.getByLabelText('RFID'));

    await userEvent.click(screen.getByText('SALVAR'));

    expect(
      screen.getByText('Número de série é obrigatório'),
    ).toBeInTheDocument();
  });
});
