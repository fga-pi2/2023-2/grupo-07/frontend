import {
  Button,
  Flex,
  Heading,
  Modal,
  ModalBody,
  ModalCloseButton,
  ModalContent,
  ModalOverlay,
  Text,
  Select,
} from '@chakra-ui/react';
import { zodResolver } from '@hookform/resolvers/zod';
import { Controller, useForm } from 'react-hook-form';

import { Input } from '@components/input';
import { useAddNewDrone } from '@pages/drones/hooks/useAddNewDrone';

import { Station } from '../../../../types/station';

import { AddDrone, addDroneSchema } from './schema';

interface AddNewDroneModalProps {
  onClose: () => void;
  reloadDrones: () => void;
  stations: Array<Station> | null;
}

export function AddNewDroneModal({
  onClose,
  reloadDrones,
  stations,
}: AddNewDroneModalProps) {
  const { isSubmitting, submit } = useAddNewDrone({
    onSuccess: () => {
      onClose();
      reloadDrones();
    },
  });

  const { control, handleSubmit, setValue } = useForm<AddDrone>({
    resolver: zodResolver(addDroneSchema),
  });

  return (
    <Modal isOpen onClose={onClose} isCentered>
      <ModalOverlay backdropFilter="auto" backdropBlur="4px" />

      <ModalContent
        display="flex"
        alignItems="center"
        justifyContent="center"
        gap="20px"
        flexDir="column"
        padding="56px 72px"
        w="100vw"
        maxW="720px"
      >
        <ModalCloseButton
          bg="#4FD1C5"
          borderRadius="50%"
          color="white"
          fontWeight={700}
          data-testid="close-btn"
        />
        <ModalBody w="full" maxW="350px">
          <Heading
            color="#4FD1C5"
            textAlign="center"
            fontSize="2rem"
            fontWeight="700"
            lineHeight="130%"
          >
            Adicionar Drone
          </Heading>

          <Text
            color="#A0AEC0"
            textAlign="center"
            fontSize=".875rem"
            fontWeight="700"
            lineHeight="140%"
            mt=".5rem"
          >
            Adicionar novo drone.
          </Text>

          <Flex
            as="form"
            flexDir="column"
            my="1.25rem"
            gap="1rem"
            onSubmit={handleSubmit(submit)}
          >
            <Controller
              name="serie"
              control={control}
              defaultValue=""
              render={({ field, fieldState: { error } }) => (
                <Input
                  label="RFID"
                  placeholder="30322073666942036461"
                  onChange={(e) => {
                    field.onChange(e);
                    setValue('serie', e.target.value);
                  }}
                  value={field.value}
                  error={!!error}
                  errorMessage={error?.message}
                />
              )}
            />

            <Controller
              name="id_base"
              control={control}
              defaultValue=""
              render={({ field }) => (
                <Select
                  placeholder="Selecione a Base"
                  onChange={(e) => {
                    field.onChange(e);
                    setValue('id_base', e.target.value);
                  }}
                  value={field.value}
                >
                  {stations?.map((item) => (
                    <option key={item.id_base} value={item.id_base}>
                      {item.name}
                    </option>
                  ))}
                </Select>
              )}
            />

            <Button
              bg="#4FD1C5"
              _hover={{ bg: '#1d726a' }}
              boxShadow="0px 3.5px 5.5px 0px rgba(0, 0, 0, 0.02)"
              color="white"
              textAlign="center"
              fontSize=".625rem"
              fontWeight="700"
              lineHeight="150%"
              borderRadius=".75rem"
              display="flex"
              alignItems="center"
              justifyContent="center"
              padding="0 .5rem"
              maxW="350px"
              w="full"
              mt=".375rem"
              type="submit"
              isLoading={isSubmitting}
              isDisabled={isSubmitting}
            >
              SALVAR
            </Button>
          </Flex>
        </ModalBody>
      </ModalContent>
    </Modal>
  );
}
