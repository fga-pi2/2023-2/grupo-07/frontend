import { z } from 'zod';

export const addDroneSchema = z.object({
  serie: z
    .string({ required_error: 'Número de série é obrigatório' })
    .min(1, 'Número de série deve ter pelo menos 1 caractere'),

  id_base: z
    .string({ required_error: 'A seleção da base é obrigatória' })
    .min(1, 'A seleção da base é obrigatória'),
});

export type AddDrone = z.infer<typeof addDroneSchema>;
