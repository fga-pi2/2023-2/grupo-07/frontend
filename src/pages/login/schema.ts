import { z } from 'zod';

export const loginNewUserSchema = z.object({
  username: z
    .string({ required_error: 'Username é obrigatório' })
    .min(1, 'Username é obrigatório'),
  password: z
    .string({ required_error: 'Senha é obrigatório' })
    .min(1, 'Senha é obrigatório'),
});

export type LoginNewUser = z.infer<typeof loginNewUserSchema>;
