import qs from 'querystring';

import { useState } from 'react';
import { toast } from 'react-toastify';

import { api } from '@config/lib/axios/api';

interface LoginProps {
  onSuccess?: (token: string) => void;
}

export function useLogin(props?: LoginProps) {
  const { onSuccess } = props ?? {};

  const [isSubmitting, setIsSubmitting] = useState(false);

  async function login(values) {
    setIsSubmitting(true);

    try {
      const formData = qs.stringify(values);
      const response = await api.post('/users/login', formData, {
        headers: {
          'Content-Type': 'application/x-www-form-urlencoded',
        },
      });

      toast.success('Login com sucesso!');

      onSuccess?.(response.data.access_token);
    } catch (err) {
      toast.error('Login falhou. Tente Novamente!');
    } finally {
      setIsSubmitting(false);
    }
  }

  return {
    isSubmitting,
    login,
  };
}
