import {
  Box,
  Button,
  Flex,
  FormControl,
  FormLabel,
  Input,
  Stack,
  Heading,
  Text,
  Image,
  Link,
  ChakraProvider,
  extendTheme,
} from '@chakra-ui/react';
import { useForm } from 'react-hook-form';
import { useNavigate } from 'react-router-dom';

import { useLogin } from '@pages/login/hooks/useLogin';
import useAuthentication from 'hooks/useAuthentication';

import Logo from '../../assets/logo-signin.svg';
import SignIn from '../../assets/signin.svg';

const theme = extendTheme({
  styles: {
    global: {
      body: {
        margin: 0,
      },
    },
  },
});

export function Login() {
  const { handleSubmit, register } = useForm();

  const onSubmit = async (data) => {
    login(data);
  };

  const navigate = useNavigate();
  const { loginUser } = useAuthentication();
  const { isSubmitting, login } = useLogin({
    onSuccess: (token: string) => {
      loginUser(token);
      navigate('/');
    },
  });

  return (
    <ChakraProvider theme={theme} resetCSS>
      <Flex justify="center" height="100vh">
        <Box
          width="50%"
          display="inline-flex"
          flexDirection="column"
          justifyContent="center"
          alignItems="center"
        >
          <Box width="353.5px" height="70px">
            <Heading
              width="350px"
              height="42px"
              flexShrink={0}
              color="var(--teal-teal-300, #4FD1C5)"
              textAlign="center"
              fontSize="32px"
              fontStyle="normal"
              fontWeight={700}
              lineHeight="130%" /* 41.6px */
            >
              Bem-vindo
            </Heading>

            <Text
              width="350px"
              height="20px"
              flexShrink={0}
              color="var(--gray-gray-400, #A0AEC0)"
              textAlign="center"
              fontSize="14px"
              fontStyle="normal"
              fontWeight={700}
              lineHeight="140%" /* 19.6px */
            >
              Coloque seu email e senha para acessar.
            </Text>
          </Box>

          <form onSubmit={handleSubmit(onSubmit)}>
            <Flex direction="column" align="flex-start" gap={25}>
              <Stack direction="column" align="flex-start" spacing={15}>
                <Stack direction="column" align="flex-start" spacing={5}>
                  <FormControl w="350px" h="75px">
                    <FormLabel
                      w="271px"
                      h="19px"
                      flexShrink={0}
                      color="var(--gray-gray-700, #2D3748)"
                      fontSize="14px"
                      fontStyle="normal"
                      fontWeight={400}
                      lineHeight="140%" /* 19.6px */
                    >
                      Email
                    </FormLabel>
                    <Input
                      type="email"
                      placeholder="Email"
                      {...register('username', {
                        required: 'Campo obrigatório',
                      })}
                      display="flex"
                      w="350px"
                      h="50px"
                      padding="0px 20px"
                      alignItems="center"
                      flexShrink={0}
                      borderRadius="15px"
                      border="1px solid var(--gray-gray-200, #E2E8F0)"
                      background="var(--black-amp-white-white, #FFF)"
                    />
                  </FormControl>

                  <FormControl w="350px" h="75px">
                    <FormLabel
                      w="271px"
                      h="19px"
                      flexShrink={0}
                      color="var(--gray-gray-700, #2D3748)"
                      fontSize="14px"
                      fontStyle="normal"
                      fontWeight={400}
                      lineHeight="140%" /* 19.6px */
                    >
                      Senha
                    </FormLabel>
                    <Input
                      type="password"
                      placeholder="Senha"
                      {...register('password', {
                        required: 'Campo obrigatório',
                      })}
                      display="flex"
                      w="350px"
                      h="50px"
                      padding="0px 20px"
                      alignItems="center"
                      flexShrink={0}
                      borderRadius="15px"
                      border="1px solid var(--gray-gray-200, #E2E8F0)"
                      background="var(--black-amp-white-white, #FFF)"
                    />
                  </FormControl>
                </Stack>
              </Stack>

              <Box
                display="flex"
                flexDirection="column"
                justifyContent="center"
                alignItems="center"
                gap="5px"
              >
                <Button
                  colorScheme="teal"
                  isLoading={isSubmitting}
                  isDisabled={isSubmitting}
                  loadingText="Autenticando..."
                  type="submit"
                  mt={4}
                  borderRadius="12px"
                  bg="var(--teal-teal-300, #4FD1C5)"
                  display="flex"
                  width="350px"
                  height="45px"
                  padding="0px 8px"
                  justifyContent="center"
                  alignItems="center"
                >
                  Entrar
                </Button>

                <Stack>
                  <Text
                    width="234px"
                    height="20px"
                    color="var(--gray-gray-400, #A0AEC0)"
                    textAlign="center"
                    fontFamily="Montserrat"
                    fontSize="14px"
                    fontStyle="normal"
                    fontWeight={400}
                    lineHeight="140%" /* 19.6px */
                  >
                    Não tem uma conta?{' '}
                    <Link
                      color="var(--teal-teal-300, #4FD1C5)"
                      fontFamily="Montserrat"
                      fontSize="14px"
                      fontStyle="normal"
                      fontWeight={700}
                      lineHeight="140%"
                      href="/cadastro"
                    >
                      Crie uma
                    </Link>
                  </Text>
                </Stack>
              </Box>
            </Flex>
          </form>
        </Box>
        <Box
          position="absolute"
          left="50%"
          transform="translateX(-50%)"
          width="609px"
          height="180px"
          zIndex={1}
          borderRadius="15px"
        >
          <Image
            src={Logo}
            alt="Logo"
            width="609px"
            height="180px"
            objectFit="cover"
            borderRadius="15px"
            mt="3.25rem"
          />
        </Box>

        <Box
          width="50%"
          height="975.848px"
          flexShrink={0}
          borderRadius="0px 0px 0px 25px"
          background="var(--primary-teal-300, #5CE1CA)"
          display="flex"
          alignItems="center"
          justifyContent="center"
        >
          <Image
            src={SignIn}
            alt="SignIn"
            bg="#5CE1CA"
            width="714px"
            height="680.939px"
          />
        </Box>
      </Flex>
    </ChakraProvider>
  );
}
