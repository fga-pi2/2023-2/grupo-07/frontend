import 'leaflet/dist/leaflet.css';

import { Box, Flex, Heading, Spinner } from '@chakra-ui/react';
import { useEffect, useState } from 'react';
import { MapContainer, Marker, Popup, TileLayer } from 'react-leaflet';

import { ImageHeader } from '@components/image-header';
import { useGetStations } from '@pages/stations/hooks/useGetStations';

import StationsHeader from '../../assets/stations-header.svg';
import { Sidebar } from '../../components/sidebar';
import { Station } from '../../types/station';

export function Map() {
  const [stations, setStations] = useState<Station[] | null>(null);

  const { isLoading, handleLoadStations } = useGetStations({ setStations });

  useEffect(() => {
    handleLoadStations();
  }, [handleLoadStations]);

  return (
    <Flex gap="2rem">
      <Sidebar />
      <Box as="main" w="full">
        <Flex flexDir="column" alignItems="center">
          <ImageHeader
            image={StationsHeader}
            pageName="Mapa"
            imageAlt="Bases"
            width="580px"
            height="432px"
          />

          <Heading
            mt="5.25rem"
            mb="1.5rem"
            color="#2D3748"
            fontSize="1.5rem"
            lineHeight="140%"
            textAlign="left"
            w="full"
          >
            Mapa - Bases
          </Heading>

          {!isLoading ? (
            <MapContainer
              center={[-15.98924, -48.044167]}
              zoom={16}
              scrollWheelZoom={false}
              style={{ height: '35rem', width: '100%' }}
            >
              <TileLayer
                attribution='&copy; <a href="https://www.openstreetmap.org/copyright">OpenStreetMap</a> contributors'
                url="https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png"
              />
              {stations && stations.length
                ? stations.map((station) => (
                    <Marker
                      position={[
                        Number(station.latitude),
                        Number(station.longitude),
                      ]}
                    >
                      <Popup>
                        {station.name} - {station.status}
                      </Popup>
                    </Marker>
                  ))
                : null}
            </MapContainer>
          ) : (
            <Spinner size="lg" colorScheme="teal" />
          )}
        </Flex>
      </Box>
    </Flex>
  );
}
