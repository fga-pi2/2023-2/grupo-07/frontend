import {
  Box,
  Button,
  Flex,
  FormControl,
  FormLabel,
  Input,
  Text,
  Image,
} from '@chakra-ui/react';
import { useForm } from 'react-hook-form';
import { useNavigate } from 'react-router-dom';
import { toast } from 'react-toastify';

import { useSignUp } from '@pages/signup/hooks/useSignUp';

import SignUpLogo from '../../assets/signuplogo.svg';

export function SignUp() {
  const { handleSubmit, register } = useForm();

  const navigate = useNavigate();
  const { isSubmitting, signUp } = useSignUp({
    onSuccess: () => {
      navigate('/entrar');
    },
  });

  const onSubmit = async (data) => {
    if (data.password !== data.repeatPassword) {
      toast.error('Senhas não coincidem. Tente Novamente!');
    } else {
      delete data.repeatPassword;
      signUp(data);
    }
  };

  return (
    <Flex flexDir="column" align="center">
      <Box
        width="100%"
        height="520.5px"
        display="inline-flex"
        flexDirection="column"
        alignItems="center"
        bg="#5CE1CA"
        borderRadius="15px"
      >
        <Image
          src={SignUpLogo}
          alt="Drones"
          width="198px"
          height="22px"
          marginTop="17px"
        />

        <Text
          width="192px"
          height="41px"
          color="var(--black-amp-white-white, #FFF)"
          textAlign="center"
          fontSize="32px"
          fontStyle="normal"
          fontWeight="700"
          lineHeight="130%" /* 41.6px */
          marginTop="62px"
        >
          Bem Vindo
        </Text>

        <Text
          width="333px"
          height="37px"
          color="var(--black-amp-white-white, #FFF)"
          textAlign="center"
          fontFamily="Montserrat"
          fontSize="14px"
          fontStyle="normal"
          fontWeight="400"
          lineHeight="140%" /* 19.6px */
        >
          Faça seu cadastro e comece agora mesmo a analisar os dados dos seus
          drones e bases.
        </Text>
      </Box>

      <Box
        width="450px"
        borderRadius="15px"
        background="var(--black-amp-white-white, #FFF)"
        boxShadow="0px 7px 23px 0px rgba(0, 0, 0, 0.05)"
        position="absolute"
        marginTop="272px"
      >
        <Box
          display="flex"
          flexDirection="column"
          justifyContent="center"
          alignItems="center"
          gap="5px"
        >
          <Box
            display="flex"
            flexDirection="column"
            alignItems="flex-start"
            gap="25px"
            marginTop="40px"
          >
            <form onSubmit={handleSubmit(onSubmit)}>
              <FormControl>
                <FormLabel
                  width="271px"
                  height="19px"
                  flexShrink={0}
                  color="var(--gray-gray-700, #2D3748)"
                  fontSize="14px"
                  fontStyle="normal"
                  fontWeight="400"
                  lineHeight="140%" /* 19.6px */
                >
                  Username
                </FormLabel>
                <Input
                  type="text"
                  placeholder="Username"
                  display="flex"
                  width="350px"
                  height="50px"
                  padding="0px 20px"
                  alignItems="center"
                  flexShrink={0}
                  borderRadius="15px"
                  border="1px solid var(--gray-gray-200, #E2E8F0)"
                  background="var(--black-amp-white-white, #FFF)"
                  {...register('username', {
                    required: 'Campo obrigatório',
                  })}
                />
              </FormControl>

              <FormControl>
                <FormLabel
                  width="271px"
                  height="19px"
                  flexShrink={0}
                  color="var(--gray-gray-700, #2D3748)"
                  fontSize="14px"
                  fontStyle="normal"
                  fontWeight="400"
                  lineHeight="140%" /* 19.6px */
                  marginTop="5px"
                >
                  Email
                </FormLabel>
                <Input
                  type="email"
                  placeholder="Email"
                  display="flex"
                  width="350px"
                  height="50px"
                  padding="0px 20px"
                  alignItems="center"
                  flexShrink={0}
                  borderRadius="15px"
                  border="1px solid var(--gray-gray-200, #E2E8F0)"
                  background="var(--black-amp-white-white, #FFF)"
                  {...register('email', {
                    required: 'Campo obrigatório',
                  })}
                />
              </FormControl>

              <FormControl>
                <FormLabel
                  width="271px"
                  height="19px"
                  flexShrink={0}
                  color="var(--gray-gray-700, #2D3748)"
                  fontSize="14px"
                  fontStyle="normal"
                  fontWeight="400"
                  lineHeight="140%" /* 19.6px */
                  marginTop="5px"
                >
                  Telefone
                </FormLabel>
                <Input
                  type="phone"
                  placeholder="Telefone"
                  display="flex"
                  width="350px"
                  height="50px"
                  padding="0px 20px"
                  alignItems="center"
                  flexShrink={0}
                  borderRadius="15px"
                  border="1px solid var(--gray-gray-200, #E2E8F0)"
                  background="var(--black-amp-white-white, #FFF)"
                  {...register('phone', {
                    required: 'Campo obrigatório',
                  })}
                />
              </FormControl>

              <FormControl>
                <FormLabel
                  width="271px"
                  height="19px"
                  flexShrink={0}
                  color="var(--gray-gray-700, #2D3748)"
                  fontSize="14px"
                  fontStyle="normal"
                  fontWeight="400"
                  lineHeight="140%" /* 19.6px */
                  marginTop="5px"
                >
                  Senha
                </FormLabel>
                <Input
                  type="password"
                  placeholder="Senha"
                  display="flex"
                  width="350px"
                  height="50px"
                  padding="0px 20px"
                  alignItems="center"
                  flexShrink={0}
                  borderRadius="15px"
                  border="1px solid var(--gray-gray-200, #E2E8F0)"
                  background="var(--black-amp-white-white, #FFF)"
                  {...register('password', {
                    required: 'Campo obrigatório',
                  })}
                />
              </FormControl>

              <FormControl>
                <FormLabel
                  width="271px"
                  height="19px"
                  flexShrink={0}
                  color="var(--gray-gray-700, #2D3748)"
                  fontSize="14px"
                  fontStyle="normal"
                  fontWeight="400"
                  lineHeight="140%" /* 19.6px */
                  marginTop="5px"
                >
                  Repita a Senha
                </FormLabel>
                <Input
                  type="password"
                  placeholder="Repita sua senha"
                  display="flex"
                  width="350px"
                  height="50px"
                  padding="0px 20px"
                  alignItems="center"
                  flexShrink={0}
                  borderRadius="15px"
                  border="1px solid var(--gray-gray-200, #E2E8F0)"
                  background="var(--black-amp-white-white, #FFF)"
                  {...register('repeatPassword', {
                    required: 'Campo obrigatório',
                  })}
                />
              </FormControl>

              <Button
                colorScheme="teal"
                type="submit"
                width="100%"
                isLoading={isSubmitting}
                isDisabled={isSubmitting}
                loadingText="Autenticando..."
                marginTop="30px"
                marginBottom="48px"
                height="45px"
                padding="0px 8px"
                justifyContent="center"
                alignItems="center"
                borderRadius="12px"
                background="var(--teal-teal-300, #4FD1C5)"
              >
                Cadastrar
              </Button>
            </form>
          </Box>
        </Box>
      </Box>
    </Flex>
  );
}
