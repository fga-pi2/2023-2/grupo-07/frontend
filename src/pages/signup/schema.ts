import { z } from 'zod';

export const signUpNewUserSchema = z.object({
  username: z
    .string({ required_error: 'Username é obrigatório' })
    .min(1, 'Username é obrigatório'),
  email: z
    .string({ required_error: 'Email é obrigatório' })
    .min(1, 'Email é obrigatório'),
  phone: z
    .string({ required_error: 'Telefone é obrigatório' })
    .min(1, 'Telefone é obrigatório'),
  password: z
    .string({ required_error: 'Senha é obrigatório' })
    .min(1, 'Senha é obrigatório'),
  repeatPassword: z
    .string({ required_error: 'Senha é obrigatório' })
    .min(1, 'Senha é obrigatório'),
});

export type SignUpNewUser = z.infer<typeof signUpNewUserSchema>;
