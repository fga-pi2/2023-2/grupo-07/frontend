import { useState } from 'react';
import { toast } from 'react-toastify';

import { api } from '@config/lib/axios/api';

interface SignUpProps {
  onSuccess?: () => void;
}

export function useSignUp(props?: SignUpProps) {
  const { onSuccess } = props ?? {};

  const [isSubmitting, setIsSubmitting] = useState(false);

  async function signUp(values) {
    setIsSubmitting(true);

    try {
      await api.post('/users/register', values, {
        headers: {
          'Content-Type': 'application/json',
        },
      });

      toast.success('Registrado com sucesso!');

      onSuccess?.();
    } catch (err) {
      toast.error('Registro falhou. Tente Novamente!');
    } finally {
      setIsSubmitting(false);
    }
  }

  return {
    isSubmitting,
    signUp,
  };
}
