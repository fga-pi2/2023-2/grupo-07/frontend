import { ChakraProvider } from '@chakra-ui/react';
import { RouterProvider } from 'react-router-dom';
import { ToastContainer } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';

import { publicRouter, privateRouter } from './router';
import { theme } from './styles/theme';
import { useAuthentication } from 'hooks/useAuthentication';

export function App() {
  const { isAuthenticated } = useAuthentication();

  return (
    <ChakraProvider theme={theme}>
      <RouterProvider router={isAuthenticated ? privateRouter : publicRouter} />
      <ToastContainer />
    </ChakraProvider>
  );
}
