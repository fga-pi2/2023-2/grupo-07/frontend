import { createBrowserRouter } from 'react-router-dom';

import { Dashboard } from '../pages/dashboard';
import { Drones } from '../pages/drones';
import { Login } from '../pages/login';
import { Profile } from '../pages/profile';
import { SignUp } from '../pages/signup';
import { Stations } from '../pages/stations';
import { Map } from '@pages/map';

export enum ROUTER_PATHS {
  'DEFAULT' = '*', // default case, if path is not defined
  'DASHBOARD' = '/',
  'LOGIN' = '/entrar',
  'SIGNUP' = '/cadastro',
  'PROFILE' = '/perfil',
  'DRONES' = '/drones',
  'STATIONS' = '/bases',
  'MAP' = '/mapa',
}

const PublicRoutes = [
  { path: '*', element: <Login /> },
  {
    path: ROUTER_PATHS.LOGIN,
    element: <Login />,
  },
  {
    path: ROUTER_PATHS.SIGNUP,
    element: <SignUp />,
  },
];

// Must be logged in
const PrivateRoutes = [
  { path: '*', element: <Dashboard /> },
  {
    path: ROUTER_PATHS.DASHBOARD,
    element: <Dashboard />,
  },
  {
    path: ROUTER_PATHS.PROFILE,
    element: <Profile />,
  },
  {
    path: ROUTER_PATHS.DRONES,
    element: <Drones />,
  },
  {
    path: ROUTER_PATHS.STATIONS,
    element: <Stations />,
  },
  {
    path: ROUTER_PATHS.MAP,
    element: <Map />,
  },
];

export const publicRouter = createBrowserRouter(PublicRoutes);

export const privateRouter = createBrowserRouter(PrivateRoutes);
