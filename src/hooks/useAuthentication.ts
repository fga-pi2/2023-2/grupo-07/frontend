import { useState, useEffect } from 'react';

export function useAuthentication() {
  const [isAuthenticated, setIsAuthenticated] = useState(false);

  const checkAuthentication = () => {
    const token = localStorage.getItem('@DRONE_PI2:token');

    if (!token) {
      // No token found, user is not authenticated
      setIsAuthenticated(false);
      return;
    }

    try {
      const decodedToken = JSON.parse(atob(token.split('.')[1])); // Decode the token payload

      const expirationTime = decodedToken.exp * 1000; // Convert expiration time to milliseconds

      // Check if the token is expired
      const isTokenNotExpired = expirationTime > Date.now();
      setIsAuthenticated(isTokenNotExpired);
    } catch (error) {
      // Error decoding or parsing the token, consider the user as not authenticated
      setIsAuthenticated(false);
    }
  };

  useEffect(() => {
    // Initial check when the component mounts
    checkAuthentication();

    // Set up a periodic check or perform other actions as needed
    const interval = setInterval(checkAuthentication, 60000); // Check every minute

    // Clean up the interval when the component unmounts
    return () => clearInterval(interval);
  }, []); // Empty dependency array to run the effect only once on mount

  // login user by adding the token to storage
  const loginUser = (token: string) => {
    localStorage.setItem('@DRONE_PI2:token', token);
    setIsAuthenticated(true);

    window.location.reload(); // hard reload to re render routes
  };

  // logout user by removing the token from storage
  const logoutUser = () => {
    localStorage.removeItem('@DRONE_PI2:token');
    setIsAuthenticated(false);

    window.location.reload(); // hard reload to re render routes
  };

  return { isAuthenticated, checkAuthentication, loginUser, logoutUser };
}

export default useAuthentication;
