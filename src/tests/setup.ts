import { afterAll, afterEach, beforeAll, vi } from 'vitest';

import { server } from './server';

import '@testing-library/jest-dom';

vi.mock('@config/lib/axios/api', async () => {
  const { mockedAxiosClient } = await import('./mocks/lib/axios');

  return { api: mockedAxiosClient };
});

// Establish API mocking before all tests.
beforeAll(() => {
  server.listen();
});

// Reset any request handlers that we may add during the tests,
// so they don't affect other tests.
afterEach(() => {
  window.history.pushState({}, 'Test page', '/');
  server.resetHandlers();
});

// Clean up after the tests are finished.
afterAll(() => server.close());
