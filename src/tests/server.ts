import type { DefaultBodyType } from 'msw';
import { rest } from 'msw';
import { setupServer } from 'msw/node';

import type { MockRequestProps } from './types';

export const TEST_BASE_URL = 'http://localhost:3000';

export const server = setupServer();

export const mockGetRequest = <T extends DefaultBodyType>({
  endpoint,
  status = 200,
  response,
  delay,
}: MockRequestProps<T>) => {
  const url = endpoint.includes('https://')
    ? endpoint
    : `${TEST_BASE_URL}${endpoint}`;

  server.use(
    rest.get(url, (_req, res, ctx) => {
      if (delay) {
        ctx.delay(delay);
      }

      return res(ctx.status(status), ctx.json(response));
    }),
  );
};

export const mockPostRequest = <T extends DefaultBodyType>({
  endpoint,
  status = 200,
  response,
  delay,
}: MockRequestProps<T>) => {
  const url = endpoint.includes('https://')
    ? endpoint
    : `${TEST_BASE_URL}${endpoint}`;

  server.use(
    rest.post(url, (_req, res, ctx) => {
      if (delay) {
        ctx.delay(delay);
      }

      return res(ctx.status(status), ctx.json(response));
    }),
  );
};

export const mockPutRequest = <T extends DefaultBodyType>({
  endpoint,
  status = 200,
  response,
  delay,
}: MockRequestProps<T>) => {
  const url = endpoint.includes('https://')
    ? endpoint
    : `${TEST_BASE_URL}${endpoint}`;

  server.use(
    rest.put(url, (_req, res, ctx) => {
      if (delay) {
        ctx.delay(delay);
      }

      return res(ctx.status(status), ctx.json(response));
    }),
  );
};

export const mockPatchRequest = <T extends DefaultBodyType>({
  endpoint,
  status = 200,
  response,
  delay,
}: MockRequestProps<T>) => {
  const url = endpoint.includes('https://')
    ? endpoint
    : `${TEST_BASE_URL}${endpoint}`;

  server.use(
    rest.patch(url, (_req, res, ctx) => {
      if (delay) {
        ctx.delay(delay);
      }

      return res(ctx.status(status), ctx.json(response));
    }),
  );
};

export const mockDeleteRequest = <T extends DefaultBodyType>({
  endpoint,
  status = 200,
  response,
  delay,
}: MockRequestProps<T>) => {
  const url = endpoint.includes('https://')
    ? endpoint
    : `${TEST_BASE_URL}${endpoint}`;

  server.use(
    rest.delete(url, (_req, res, ctx) => {
      if (delay) {
        ctx.delay(delay);
      }

      return res(ctx.status(status), ctx.json(response));
    }),
  );
};
