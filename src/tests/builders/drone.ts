import { faker } from '@faker-js/faker';
import { build, oneOf, perBuild } from '@jackfranklin/test-data-bot';

import { Drone } from '../../types/drone';

export const droneBuilder = build<Drone>({
  fields: {
    id_drone: perBuild(() => faker.number.int()),
    serie: perBuild(() => faker.string.sample()),
    status: oneOf('ACTIVE', 'INACTIVE'),
    id_base: perBuild(() => faker.number.int()),
    id_user: perBuild(() => faker.number.int()),
    created_at: perBuild(() => faker.date.anytime().toISOString()),
  },
});
