import { faker } from '@faker-js/faker';
import { build, oneOf, perBuild } from '@jackfranklin/test-data-bot';

import { Station } from '../../types/station';

export const stationBuilder = build<Station>({
  fields: {
    name: perBuild(() => faker.lorem.word()),
    latitude: perBuild(() => String(faker.location.latitude())),
    longitude: perBuild(() => String(faker.location.longitude())),
    is_active: oneOf(true, false),
    status: oneOf('ACTIVE', 'INACTIVE'),
    id_base: perBuild(() => faker.number.int()),
    id_user: perBuild(() => faker.number.int()),
    created_at: perBuild(() => faker.date.anytime().toISOString()),
  },
});
