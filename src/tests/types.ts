import type { DelayMode } from 'msw';

export interface MockRequestProps<T> {
  endpoint: string;
  status?: number;
  response: T;
  delay?: number | DelayMode;
}
