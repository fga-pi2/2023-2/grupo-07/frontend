import axios from 'axios';

import { TEST_BASE_URL } from '../../server';

export const mockedAxiosClient = axios.create({ baseURL: TEST_BASE_URL });
