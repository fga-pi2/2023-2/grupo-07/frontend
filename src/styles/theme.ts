import { extendTheme, theme as baseTheme } from '@chakra-ui/react';

export const theme = extendTheme({
  ...baseTheme,
  colors: {
    brand: {
      500: '#5CE1CA',
    },
  },
  fonts: {
    heading: `'Montserrat', sans-serif`,
    body: `'Montserrat', sans-serif`,
    color: '#2D3748',
  },
  styles: {
    global: {
      body: {
        bg: '#F8F9FA',
        ml: '2rem',
        mr: '1.5rem',
        my: '2rem',
      },
    },
  },
});
