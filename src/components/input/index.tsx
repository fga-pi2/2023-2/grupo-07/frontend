/* eslint-disable react/jsx-props-no-spreading */
import {
  FormControl,
  FormLabel,
  Input as ChakraInput,
  FormErrorMessage,
} from '@chakra-ui/react';
import { ComponentPropsWithRef } from 'react';

interface InputProps extends Omit<ComponentPropsWithRef<'input'>, 'size'> {
  required?: boolean;
  error?: boolean;
  errorMessage?: string;
  label?: string;
}

export function Input({
  required,
  error,
  errorMessage,
  placeholder,
  label,
  ...props
}: InputProps) {
  return (
    <FormControl w="full" isInvalid={error} isRequired={required ?? false}>
      {label ? <FormLabel mb=".25rem">{label}</FormLabel> : null}

      <ChakraInput
        w="full"
        maxW="350px"
        height="3.125rem"
        padding="0 1.25rem"
        fontSize="14px"
        fontWeight="400"
        _placeholder={{ color: '#A0AEC0' }}
        placeholder={placeholder}
        focusBorderColor="#4FD1C5"
        {...props}
      />
      {error ? (
        <FormErrorMessage>{errorMessage ?? 'Campo inválido'}</FormErrorMessage>
      ) : null}
    </FormControl>
  );
}
