import { Button, Divider, Flex, Image, Text } from '@chakra-ui/react';
import { Link, useLocation } from 'react-router-dom';

import ChargeSvg from '../../assets/charge.svg';
import CircleSvg from '../../assets/circle.svg';
import DashboardSvg from '../../assets/dashboard.svg';
import DroneSvg from '../../assets/drone.svg';
import LogoSvg from '../../assets/logo.svg';
import ProfileSvg from '../../assets/profile.svg';
import MapSvg from '../../assets/map.svg';
import { ROUTER_PATHS } from '../../router';

interface MenuItemProps {
  to: string;
  image: string;
  label: string;
  isCurrentPath: boolean;
}

function MenuItem({ to, image, label, isCurrentPath }: MenuItemProps) {
  const bgStyles = isCurrentPath ? 'white' : 'transparent';
  const boxShadowStyles = isCurrentPath
    ? '0px 3.5px 5.5px 0px rgba(0, 0, 0, 0.02)'
    : 'none';
  const borderRadiusStyles = isCurrentPath ? '1rem' : 0;
  const paddingStyles = isCurrentPath ? '10px 48px 10px 16px' : '0';

  return (
    <Link to={to}>
      <Flex
        bg={bgStyles}
        alignItems="center"
        gap=".5rem"
        borderRadius={borderRadiusStyles}
        boxShadow={boxShadowStyles}
        padding={paddingStyles}
      >
        <Image src={image} />

        <Text as="strong" color="#A0AEC0">
          {label}
        </Text>
      </Flex>
    </Link>
  );
}

function Documentation() {
  return (
    <Flex
      flexDir="column"
      w="full"
      p="1rem .875rem"
      gap="1rem"
      borderRadius="1rem"
      bg="#4FD1C5"
      boxShadow="0px 3.5px 5.5px 0px rgba(0, 0, 0, 0.02)"
      position="fixed"
      maxW="192px"
      bottom="2rem"
      left="2rem"
    >
      <Flex
        alignItems="center"
        justifyContent="center"
        bg="white"
        w="35px"
        h="35px"
        borderRadius="12px"
      >
        <Image src={CircleSvg} w="24px" h="24px" />
      </Flex>

      <Flex flexDir="column">
        <Text as="strong" color="white" fontSize=".875rem" lineHeight="140%">
          Precisa de ajuda?
        </Text>
        <Text as="span" color="white" fontSize="12px" lineHeight="150%">
          Veja nossa documentação
        </Text>
      </Flex>

      <Button
        borderRadius="12px"
        bg="white"
        display="flex"
        alignItems="center"
        justifyContent="center"
        px=".5rem"
        color="black"
        textAlign="center"
        fontSize=".75rem"
        lineHeight="150%"
      >
        Documentação
      </Button>
    </Flex>
  );
}

export function Sidebar() {
  const location = useLocation();
  const currentRouterPath = location.pathname;

  return (
    <Flex
      flexDir="column"
      maxW="234px"
      justifyContent="space-between"
      alignItems="center"
    >
      <Flex flexDir="column" gap="1rem">
        <Image src={LogoSvg} />
        <Divider my="1rem" />

        <MenuItem
          to="/"
          image={DashboardSvg}
          label="Dashboard"
          isCurrentPath={currentRouterPath === ROUTER_PATHS.DASHBOARD}
        />

        <MenuItem
          to="/perfil"
          image={ProfileSvg}
          label="Perfil"
          isCurrentPath={currentRouterPath === ROUTER_PATHS.PROFILE}
        />

        <MenuItem
          to="/drones"
          image={DroneSvg}
          label="Drones"
          isCurrentPath={currentRouterPath === ROUTER_PATHS.DRONES}
        />

        <MenuItem
          to="/bases"
          image={ChargeSvg}
          label="Bases"
          isCurrentPath={currentRouterPath === ROUTER_PATHS.STATIONS}
        />

        <MenuItem
          to="/mapa"
          image={MapSvg}
          label="Mapa"
          isCurrentPath={currentRouterPath === ROUTER_PATHS.MAP}
        />
      </Flex>

      <Documentation />
    </Flex>
  );
}
