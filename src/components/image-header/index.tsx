import { Flex, Image, Text, Button } from '@chakra-ui/react';
import useAuthentication from 'hooks/useAuthentication';
import LogoutIcon from '../../assets/logout.svg';

interface ImageHeaderProps {
  image: string;
  pageName: string;
  imageAlt?: string;
  width?: string;
  height?: string;
  marginTop?: string;
}

export function ImageHeader({
  image,
  pageName,
  imageAlt,
  width,
  height,
  marginTop,
}: ImageHeaderProps) {
  const { logoutUser } = useAuthentication();

  return (
    <Flex
      w="full"
      alignItems="center"
      h="362px"
      padding="1.5rem 1.5rem"
      flexDir="column"
      position="relative"
      bg="#4FD1C5"
      borderRadius="1rem"
    >
      <Flex justifyContent="space-between" w="full">
        <Text color="white" fontSize="12px" lineHeight="150%">
          Pages / {pageName}
        </Text>
        <Flex
          onClick={() => logoutUser()}
          cursor="pointer"
          color="white"
          fontSize="12px"
          lineHeight="150%"
          fontWeight="700"
          _hover={{
            filter: 'brightness(0.9)',
          }}
          transition="filter 0.3s ease"
        >
          <Image
            src={LogoutIcon}
            alt="Logout"
            marginRight="0.3rem"
            color="white"
          />
          Logout
        </Flex>
      </Flex>
      <Image
        src={image}
        alt={imageAlt}
        width={width}
        height={height}
        marginTop={marginTop}
      />
    </Flex>
  );
}

ImageHeader.defaultProps = {
  imageAlt: '',
  width: 'auto',
  height: 'auto',
  marginTop: '.875rem',
};
